 # Documentación de la implementación de referencia
Repositorio con el contenido de la documentación con las instrucciones para la instalación de los componentes incluidos en la implementación de referencia. Esta documentación está pública en la plataforma [ReadTheDocs](https://impact-data-implementacion-de-refencia.readthedocs.io/es/latest/)

# Contribución
Se ha definido una guía de contribución separada de este archivo, se encuentra en el archivo [CONTRIBUTING.md](https://gitlab.bsc.es/impact-data/impd-reference-implementation-docs/-/blob/main/CONTRIBUTING.md).

# Licencia
La documentación incluida en este repositorio se licencia bajo la [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0)


# Agradecimientos
_El proyecto IMPaCT-Data (Exp. IMP/00019) ha sido financiado por el Instituto de Salud Carlos III, co-financiado por el Fondo Europeo de Desarrollo Regional (FEDER, “Una manera de hacer Europa”)._
