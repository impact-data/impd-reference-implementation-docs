# Guía para contribuir a esta documentación

Hay diversas opciones para contribuir con IMPaCT-Data:

- Envíanos tus comentarios
- Contribuye con contenido

## Envíanos tus comentarios! 

Estamos interesados en sus opiniones, puede darnos feedback añadiendo un `issue` en este repositorio o utilizar el 
enlace para contactar con nosotros que está en la [página web del proyecto](https://impact-data.bsc.es/).

## Contribuye con contenido

Se ha escogido la plataforma [ReadTheDocs](https://readthedocs.org/) para hacer pública esta documentación, con el
generador de documentación [Sphinx](https://www.sphinx-doc.org).

En este repositorio se trabaja con 2 ramas: 
* ``main``: versión pública, en esta rama no se generará contenido nuevo. Una vez publicada una versión de la documentación, solo se podrán arreglar contenido erroneo o errores tipográficos. 
* ``development`` : versión en la que se genera contenido nuevo antes de dar por cerrada una versión. Dependiendo de los permisos del usuario contribuyendo se podrá hacer ``commit`` directamente en esta rama o se deberá hacer un ``pull request``. Se recomienda trabajar en una rama de ``development`` para preparar el contenido nuevo.

Enlaces de interés:
* Formato usado para los archivos Sphinx (.rst): [https://sublime-and-sphinx-guide.readthedocs.io/](https://sublime-and-sphinx-guide.readthedocs.io/])

Por favor utilizar la [guía general de contribución del proyecto](https://gitlab.bsc.es/impact-data/impact-data/-/blob/main/CONTRIBUTING.md) 
que se incluye en el repositorio [IMPaCT-Data Hub](https://gitlab.bsc.es/impact-data/impact-data)

### Gestión de nuevo contenido

* **Antes de crear nuevo contenido**: 
  * Crear un issue nuevo (si no existe) y assignarlo a la persona que lo generará. En la descripción se incluye en nombre del archivo(s) que están relacionados
  * Añadir el nombre de la persona que generará el contenido en la lista de autores del archivo ``pyproject.toml``
* **Durante la creación del contenido**: Para facilitar el seguimiento, al hacer ``commit`` se recomienda incluir en el texto del commit el número del issue relacionado (#num).
* **Cuando el contenido se da por acabado**: Añadir comentario en el issue correspondiente y cerrarlo.

.. warning::
   El nuevo contenido se genera en la rama development. En la rama master solo se arreglan errores.

### Nueva página
Para evitar que se genere una estructura compleja de la documentación, antes de crear una nueva página se recomienda buscar si ya hay en la estructura una sección en la que se puede añadir. 

En esta documentación hay 2 tipos de páginas:
* **guias de instalación de componentes**: páginas que continen la información para desplegar 1 componente concreto. Generalmente son hojas en el arbol de contenido de la documentación. Estas páginas tienen un formato concreto, hay que utilizar la plantilla que se proporciona.
* **paginas generales**: Son la páginas que no corresponden a guías de instalación de componentes, estas páginas son de formato libre.

### Nueva sección
Las secciones que se muestran en la parte izquierda, que dan acceso al contenido de esta documentación, están definidas
en el archivo de la página de inicio de la documentación ([docs/source/index.rst](https://gitlab.bsc.es/impact-data/impd-reference-implementation-docs/-/blob/main/docs/source/index.rst)). 
Las secciones se definen como entradas de una tabla de contenidos (utilizando ``.. toctree::``), pero se ocultan para que no se 
muestren en la página de inicioTodas las entradas (utilizando ``:hidden:``).

### Sección de la documentación con 1 nivel de sub-secciones
A continuación hay un ejemplo`de una sección de la documentación en la que se ve en el menú el título de la sección "Infraestructura en la nube", 
que tiene 3 sub-secciones que corresponde directamente a contenido (no tienen sub-secciones). 

Esta tipo de secciones tienen los siguientes modificadores:
* ``:caption:``: Nombre de la sección
* ``:titlesonly:``: Solo muestra el título de la página incluido en el archivo correspondiente de la sub-sección como nombre de la sección. Si no se incluye este modificador aparecerán los subtítulos definidos en la página como sub-secciones en el menú.
* ``:hidden:``: no se muestra el TOC de esta sección en la página que se ve en el ReadTheDocs en la parte del contenido (derecha)


.. code-block:: console
  .. toctree::
    :caption: Infraestructura en la nube
    :titlesonly:
    :hidden:

        infraestructura/introduccion
        infraestructura/nodo_computacional
        infraestructura/nodo_datos

## Sección de la documentación con 2 o más niveles de sub-secciones
A continuación hay un ejemplo`de una sección de la documentación en la que se ve en el menú el título de la sección "Nodo de Datos",
que tiene 2 sub-secciones, que a su vez tienen sub-secciones. En este caso, para la página de la sección general se utilizan 
los mismos modificadores que cuando solo hay 1 nivel de sub-secciones. 

.. code-block:: console
  .. toctree::
     :caption: Nodo de Datos
     :hidden:

    nodo_datos/almacenamiento/index
    nodo_datos/descubrimiento/index


La diferencia es que en los archivos ```nodo_datos/almacenamiento/index` y ``nodo_datos/descubrimiento/index`` también 
incluye una tabla de contenido (``.. toctree::``) que son las sub-secciones que se muestran para esta sección en el menú.

## Despliegue de la rama development

** Faltan instrucciones **


