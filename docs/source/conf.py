# Configuration file for the Sphinx documentation builder.

import sys
import os
sys.path.append(os.path.abspath('..'))

# -- Project information

project = 'IMPaCT-Data'
copyright = '2023, BSC-CNS'
author = 'Lidia López / Albert Hornos'
footer_project_logo = 'static/images/logo_UE_MinisterioCienciaInnovacion_ISCIII.png'

release = '0.1'
version = '0.1.0'

# -- General configuration

extensions = [
    'sphinx.ext.duration',
    'sphinx.ext.doctest',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
    'sphinx.ext.imgconverter',
    'myst_parser',
]

myst_enable_extensions = [
    "colon_fence"
]

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'sphinx': ('https://www.sphinx-doc.org/en/master/', None),
}
intersphinx_disabled_domains = ['std']

templates_path = ['_templates']

source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'markdown',
    '.md': 'markdown',
}
# -- Options for HTML output

html_theme = 'sphinx_rtd_theme'

source_static_path = ['source/static','source/static/images','source/static/images/components']

html_logo = 'static/images/logo_impact_data_horizontal_white_250_34.png'

html_theme_options = {
    'logo_only': True,
    'display_version': False,
}

html_sidebars = { '**': ['globaltoc.html', 'relations.html', 'sourcelink.html', 'searchbox.html'] }

# -- Options for EPUB output
epub_show_urls = 'footnote'

html_show_sourcelink = False
