IMPaCT-Data Implementación de Referencia
========================================

`IMPaCT-Data <https://impact-data.bsc.es/>`_ es el el eje de Ciencia del Dato del programa `IMPaCT <https://impact.isciii.es/>`_ para crear una infraestructura de Medicina de Precisión. Su objetivo principal es apoyar el desarrollo de un sistema común, interoperable e integrado para la recogida y análisis de datos clínicos, datos moleculares (ómicos) y datos de imagen médica, contribuyendo a este fin con el conocimiento y recursos disponibles en el Sistema Español de Ciencia y Tecnología. Este desarrollo permitirá a los investigadores tener una perspectiva poblacional basada en datos individuales.

El principal objetivo de **IMPaCT-Data** es crear la primera iteración, a modo de prueba de concepto, de una infraestructura y los protocolos necesarios para coordinar, integrar, gestionar y analizar datos clínicos, genómicos y de imagen médica, proporcionando herramientas validadas para facilitar la implementación eficaz y coordinada de la Medicina de Precisión (personalizada) en el Sistema de Salud.

Dicha infraestructura y los protocolos desarrollados, facilitarán el uso secundario para investigación de los datos generados en el sistema sanitario español. Estos datos incluyen historias clínicas electrónicas (HCE), datos de imagen médica y datos depositados en repositorios genómicos. El objetivo final es contribuir al sistema sanitario con el conocimiento y metodología generado, permitiendo a los investigadores abordar preguntas de investigación utilizando los datos disponibles. Al permitir el análisis combinado de los datos individuales, proporcionando una perspectiva basada en la población, esta plataforma apoya el avance de los descubrimientos científicos y la mejora de los servicios de salud. 

El eje de Ciencia del Dato tiene una distribución nacional con un total de 49 instituciones participantes de 15 Comunidades Autónomas.

En las secciones de esta **Implementación de Referencia** podéis encontrar tanto documentos (como las **recomendaciones** sobre datos y software) y **demostradores** generales como secciones específicas para cada uno de los componentes de esta primera iteración de la infraestructura. Cada una de estas secciones incluye una breve explicación de cada componente, la documentación técnica necesaria para instalar y ejecutar el componente y los enlaces necesarios para acceder a cada uno de ellos.

Los **Componentes Centrales** son los necesarios para configurar un nodo central que albergue los servicios únicos centralizados de la federación, mientras que los **Componentes Locales** son los que necesita instalar un socio de forma local para tener acceso a las diferentes herramientas y a la propia red de datos federada.

Finalmente, la sección de **Evaluación** contiene la explicación y enlaces de los procesos de evaluación de los componentes.

.. note::
    Se ha ampliado el plazo para la evaluación de los componentes. Finalizará el **31 de Marzo de 2025**.

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Implementación de Referencia

    Implementación de Referencia <self>

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Recomendaciones

    Recomendaciones sobre datos y software <content/ref-imp/recomendations/recomendations>

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Demostradores

    Primera iteración de la nube IMPaCT-Data <content/ref-imp/demonstrators/impact-data-cloud-first-iteration>
    Autenticación y Autorización en la nube IMPaCT-Data <content/ref-imp/demonstrators/impact-data-cloud-authn-authz>
    GA4GH Beacon <content/ref-imp/demonstrators/impact-data-beacon_video>

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Integrador de datos biomedicos

    Integrador de datos biomédicos <content/ref-imp/biomedical-data-integrator/biomedical-data-integrator>

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Componentes Locales
    
    Componentes Locales <content/ref-imp/components/local/index>
    Local EGA <content/ref-imp/components/local/localega>
    Beacon v2 <content/ref-imp/components/local/beacon>
    Beacon-OMOP <content/ref-imp/components/local/beacon-omop>
    Beacon-cBioportal <content/ref-imp/components/local/beacon-cbioportal>
    Galaxy + Pulsar <content/ref-imp/components/local/pulsar>
    openVRE <content/ref-imp/components/local/openvre>
    Data Reference (CVMFS) <content/ref-imp/components/local/ref-data-client-cvmfs>
    Authentication/Authorization Client <content/ref-imp/components/local/keycloak-client>
    Repositorio de datos clínicos sintéticos <content/ref-imp/components/local/synthetic-data-repository>
    Generador de datos clínicos sintéticos <content/ref-imp/components/local/synthetic-data-generator>
    Guía para cBioportal <content/ref-imp/components/local/cbioportal-guide>
    Guía para OHDSI Tools <content/ref-imp/components/local/ohdsi-tools-guide>

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Componentes Centrales

    Componentes Centrales <content/ref-imp/components/central/index>
    Beacon Network <content/ref-imp/components/central/beacon-network>
    Galaxy Server Central <content/ref-imp/components/central/galaxy-server>
    Authentication/Authorization Server <content/ref-imp/components/central/keycloak-server>
    Guía de referencia del ENS <content/ref-imp/components/central/national-security-scheme>

.. toctree::
    :glob:
    :maxdepth: 2
    :caption: Evaluación de los componentes

    Evaluación <content/ref-imp/evaluation/evaluation>
    Fase de desarrollo del software <content/ref-imp/evaluation/software-management-plan>
    Usabilidad de los componentes <content/ref-imp/evaluation/software-usability-scale>
