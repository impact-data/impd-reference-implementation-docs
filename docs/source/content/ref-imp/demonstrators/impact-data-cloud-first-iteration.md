# Demostración de las funcionalidades iniciales de la 1ra iteración de la nube federada IMPaCT-Data

Este vídeo muestra el resultado del trabajo realizado en la primera iteración del desarrollo de la nube federada de IMPaCT-Data, como parte del paquete de trabajo dos del proyecto IMPaCT-Data.

En el vídeo se muestra el proceso de login federado y la ejecución de trabajos haciendo uso de recursos compartidos.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vzb6uUyY-AQ?si=SJTuMBJ4JgnUOGNl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>