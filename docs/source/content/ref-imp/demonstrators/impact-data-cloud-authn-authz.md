# Demostración del sistema de autenticación y autorización de la nube federada IMPaCT-Data

Este vídeo muestra parte del resultado del trabajo realizado en la primera iteración del desarrollo de la nube federada de IMPaCT-Data, como parte del paquete de trabajo dos del proyecto IMPaCT-Data.

En el vídeo se muestra la vinculación de las cuentas institucionales a la cuentas IMPaCT-Data y el uso de pasaportes y visados, definidos por la Global Alliance for Genomics and Health (GA4GH), para el acceso local a datos protegidos.


<iframe width="560" height="315" src="https://www.youtube.com/embed/gDN_3ktHYuE?si=br3yaqEdlTksbnm4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>