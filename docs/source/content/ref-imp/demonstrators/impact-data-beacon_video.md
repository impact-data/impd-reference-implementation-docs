# Presentación de la API de descubrimiento GA4GH BEACON

En un mundo hiperconectado y repleto de datos, la información genómica y clínica existente sigue siendo difícil de encontrar, pero ¿y si existiera una herramienta para localizar este tipo de datos? **GA4GH Beacon** es una API de la [Global Alliance for Genomics and Health](https://www.ga4gh.org/) (GA4GH) que ayuda a resolver este problema. Funciona como un motor de búsqueda para realizar consultas concretas sobre datos genómicos y biomédicos. 

Beacon ya se usa en diversos proyectos internacionales. Este es el caso de [IMPaCT](https://impact.isciii.es/), el programa de medicina personalizada del Instituto de Salud Carlos III que ya usa Beacon para que los profesionales puedan buscar casos concretos en España, todo ello sin poner en riesgo la privacidad de los pacientes. 

En este vídeo puedes conocer Beacon y su uso:

<iframe width="560" height="315" src="https://www.youtube.com/embed/d-8RKvn23w0?si=zzm1rdgG8BRLXsYR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


Más información sobre Beacon: [https://genomebeacons.org/](https://genomebeacons.org/)

Más información sobre IMPaCT: [https://impact.isciii.es/](https://impact.isciii.es/)

