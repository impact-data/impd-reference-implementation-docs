# Evaluación - Fase del desarrollo del software

El objetivo de esta sección es documentar y gestionar los procesos de evaluación de la prueba de despliegue (**Implementación de Referencia**) de los componentes (software) desarrollados en el seno del proyecto **IMPaCT-DAta**. 

Esta Implementación de Referencia pretende demostrar que las tecnologías, procesos y herramientas seleccionados como componentes, están preparados, son utilizables y están validados tanto internamente, por los desarrolladores, como externamente por usuarios finales. De manera que, en el futuro, solo será necesario escalar e implementar las infraestructuras necesarias, aplicando dichas tecnologías y procesos cuando se desarrolle y ejecute la infraestructura final.

Como parte del proceso de evaluación se han identificado dos metodologías: una para evaluar los componentes internamente (desarrolladores) y otra para capturar y agilizar la evaluación de los componentes por parte de los usuarios finales.

## Evaluación interna (desarrolladores)

Se usará el **Software Management Plan** de Elixir para evaluar de forma sistematizada el nivel de finalización de cada uno de los componentes.

## Evaluación externa (usuarios)

```{important}

La **evaluación** de los componentes de la Implementación de Referencia de IMPaCT-Data por parte de los usuarios se ha ampliado hasta el **31 de Marzo de 2025**

Muchas gracias por vuestra colaboración.

```

Se usará una escala validada (*System Usability Scale* **SUS**) para evaluar el grado de usabilidad percibido por los usuarios finales en cada uno de los componentes. 

Estas evaluaciones servirán para evaluar el grado de finalización de los componentes y que estos son viables, así como su nivel de aceptación por parte de los usuarios finales. Además de para probar la eficiencia de las recomendaciones del proyecto y, de ser necesario, generar una nueva versión del documento de recomendaciones sobre datos y software de IMPaCT-Data.

```{note}
Todos los resultados a fecha 11/12/2024 (última Asamblea General) de las evaluaciones (internas y externas) se reportarán en el entregable final del proyecto (E6.3 - Resultados de la Implementación de Referencia) El nuevo documento de recomendaciones se lanzará a principios de 2025.
```

