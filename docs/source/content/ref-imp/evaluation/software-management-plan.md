# Evaluación interna - Fase del desarrollo del software

Para evaluar la integridad y el nivel de finalización de cada uno de los componentes se usará el **Software Management Plan (SMP) for Life Sciences** de ELIXIR.

Enlace al **Software Management Plan (SMP)**: [https://osf.io/preprints/biohackrxiv/k8znb](https://osf.io/preprints/biohackrxiv/k8znb).

El SMP de ELIXIR define las siguientes etapas en el proceso de desarrollo del software:

1.	**Inicio**. Incluye la definición del concepto, escritura de la propuesta, planificación e inicio.
2.	**Construcción**. Incluye prototipado, construcción e implementación de las funcionalidades principales
3.	**Aplicación**. Incluye lanzamiento y evaluación de la calidad.
4.	**Producción**. Incluye el uso del software bajo condiciones del mundo real, de manera escalable y estable.
5.	**Publicación**. Publicación del software y/o de resultados de investigación obtenidos a través del uso del software.

Para realizar la evaluación de cada componente, el SMP de ELIXIR analiza los parámetros definidos en la siguiente tabla. Dichos parámetros están basados en los conceptos FAIR de gestión abierta y transparente de datos.


<table border="1" cellpadding="5">
    <tbody>
        <tr>
            <td colspan="6" style="width:100%;background-color:#2aa87e; color:#ffffff">
                <em><strong>Accesibilidad / Licencia</strong></em>
            </td>
        </tr>
        <tr>
            <td style="width:40%">
                <p>&iquest;Como pueden acceder terceros al software?</p>
            </td>
            <td colspan="5" style="width:60%">
                <ul>
                    <li>Disponible publicamente (p.ej. GitHUb, via URL, etc.)</li>
                    <li>No es relevante/aplicable a este software</li>
                    <li>Otros</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td style="width:40%">
                <p>El software sin licencia no puede ser usado por otros. &iquest;El software tiene licencia?</p>
            </td>
            <td colspan="5" style="width:60%">
                <ul>
                    <li>NO</li>
                    <li>SI</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td style="width:40%">
                <p>En caso afirmativo, &iquest;que licencia tiene el software?</p>
            </td>
            <td colspan="5" style="width:60%">
                <ul>
                    <li>Lista SPDX</li>
                    <li>Otros</li>
                    <li>N/A</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="width:100%;background-color:#2aa87e; color:#ffffff">
                <em><strong>Do</strong></em><em><strong>cumentaci&oacute;n</strong></em>
            </td>
        </tr>
        <tr>
            <td colspan="2" width="263">
                <p>&iquest;Qu&eacute; tipo de documentaci&oacute;n est&aacute; disponible, facilitada con el software y
                    entregada bajo las mismas condiciones? (por favor, incluye una URL en caso de que est&eacute;
                    disponible)</p>
            </td>
            <td colspan="4" width="330">
                <ul>
                    <li>Documentaci&oacute;n para Usuarios</li>
                    <li>Documentaci&oacute;n para Programadores</li>
                    <li>Documentaci&oacute;n APIs</li>
                    <li>Archivo README</li>
                    <li>Notas de lanzamiento (release)</li>
                    <li>Docstring/comentarios en el c&oacute;digo fuente</li>
                    <li>Changelog</li>
                    <li>Otros (para m&aacute;s de uno, por favor, a&ntilde;adir varias filas)</li>
                    <li>Ninguna</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2" width="263">
                <p>&iquest;Se especifica el prop&oacute;sito del software en la documentaci&oacute;n?</p>
            </td>
            <td colspan="4" width="330">
                <ul>
                    <li>NO</li>
                    <li>SI</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="2" rowspan="5" width="263">
                <p>&iquest;Describe la documentaci&oacute;n como...</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </td>
            <td colspan="3" width="189">
                <p>...probar el software?</p>
            </td>
            <td width="141">
                <ul>
                    <li>NO</li>
                    <li>SI</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="189">
                <p>... usar el software?</p>
            </td>
            <td width="141">
                <ul>
                    <li>NO</li>
                    <li>SI</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="189">
                <p>...construir el software?</p>
            </td>
            <td width="141">
                <ul>
                    <li>NO</li>
                    <li>SI</li>
                    <li>N/A</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="189">
                <p>...desplegar el software?</p>
            </td>
            <td width="141">
                <ul>
                    <li>NO</li>
                    <li>SI</li>
                    <li>N/A</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="189">
                <p>...instalar el software?</p>
            </td>
            <td width="141">
                <ul>
                    <li>NO</li>
                    <li>SI</li>
                    <li>N/A</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="width:100%;background-color:#2aa87e; color:#ffffff">
                <em><strong>Validaci&oacute;n (Testing)</strong></em>
            </td>
        </tr>
        <tr>
            <td colspan="3" rowspan="9" width="297">
                <p>&iquest;Qu&eacute; tipo de validaci&oacute;n se ha usado?</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
            </td>
            <td width="118">
                <p>Ninguna</p>
            </td>
            <td colspan="2" width="179">
                <ul>
                    <li>NO</li>
                    <li>SI</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="118">
                <p>Unidad</p>
            </td>
            <td colspan="2" width="179">
                <ul>
                    <li>NO</li>
                    <li>SI (Manual)</li>
                    <li>SI (Autom&aacute;tica)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="118">
                <p>Integraci&oacute;n</p>
            </td>
            <td colspan="2" width="179">
                <ul>
                    <li>NO</li>
                    <li>SI (Manual)</li>
                    <li>SI (Autom&aacute;tica)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="118">
                <p>Funcional</p>
            </td>
            <td colspan="2" width="179">
                <ul>
                    <li>NO</li>
                    <li>SI (Manual)</li>
                    <li>SI (Autom&aacute;tica)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="118">
                <p>End-to-end</p>
            </td>
            <td colspan="2" width="179">
                <ul>
                    <li>NO</li>
                    <li>SI (Manual)</li>
                    <li>SI (Autom&aacute;tica)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="118">
                <p>Linting</p>
            </td>
            <td colspan="2" width="179">
                <ul>
                    <li>NO</li>
                    <li>SI (Manual)</li>
                    <li>SI (Autom&aacute;tica)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="118">
                <p>Regresi&oacute;n</p>
            </td>
            <td colspan="2" width="179">
                <ul>
                    <li>NO</li>
                    <li>SI (Manual)</li>
                    <li>SI (Autom&aacute;tica)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="118">
                <p>Frontend-GUI</p>
            </td>
            <td colspan="2" width="179">
                <ul>
                    <li>NO</li>
                    <li>SI (Manual)</li>
                    <li>SI (Autom&aacute;tica)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td width="118">
                <p>Otros</p>
            </td>
            <td colspan="2" width="179">
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;Los datos de ejemplo&nbsp; y/o par&aacute;metros que se pueden usar para probar el software
                    est&aacute;n disponibles junto al c&oacute;digo fuente?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                    <li>N/A</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="width:100%;background-color:#2aa87e; color:#ffffff">
                <em><strong>Interoperabilidad</strong></em>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;El software utiliza formatos de input/output est&aacute;ndar y pre-existentes?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>NO</li>
                    <li>SI (parcialmente)</li>
                    <li>SI</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>En caso afirmativo (ya sea completa o parcialmente), por favor, seleccione los est&aacute;ndares que
                    est&aacute; usando de la lista.</p>
                <p>&nbsp;</p>
                <p>Si no est&aacute; listado, por favor utilice el registro FAIRsharing para proporcionar tant el nombre
                    como la URL</p>
                <p>&nbsp;</p>
                <p>Si no est&aacute; listado en FAIRsharing, por favor, facilite una URL que sea automatizable si es
                    posible.</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>Nombre (de cada est&aacute;ndar)</li>
                    <li>URL(de cada est&aacute;ndar)</li>
                    <li>Descripci&oacute;n (opcional) (de cada est&aacute;ndar)</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="width:100%;background-color:#2aa87e; color:#ffffff">
                <em><strong>Versionado</strong></em>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;El software usa un sistema de control por versiones?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;Que sistema de control por versiones usa?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>Selecci&oacute;n m&uacute;ltiple con Otros como texto</li>
                    <li>Git</li>
                    <li>Mercurial</li>
                    <li>Subversion</li>
                    <li>CVS</li>
                    <li>Otros</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;Usa versionado sem&aacute;ntico?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="width:100%;background-color:#2aa87e; color:#ffffff">
                <em><strong>Reproducibilidad</strong></em>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;provees lanzamientos (releases) del software?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;c&oacute;mo defines dependencias lenguaje-espec&iacute;ficas del software y su
                    versi&oacute;n?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>Texto</li>
                    <li>N/A</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;C&oacute;mo captura el entorno necesario para correr el software?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>Texto</li>
                    <li>N/A</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;Provee ejemplos de inputs y outputs que se puedan usar para reproducir el funcionamiento del
                    software?</p>
                <p>En caso afirmativo, &iquest;donde se puede encontrar?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                </ul>
                <p>&nbsp;</p>
                <p>URL, por favor, indique la URL si est&aacute; disponible.</p>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;Se especifica como los usuarios pueden informar de bugs y/o problemas de usabilidad?</p>
                <p>P.ej. Indicando un correo, un protocolo de mensajes directos o indicando que no hay soporte de bugs.
                </p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="width:100%;background-color:#2aa87e; color:#ffffff">
                <em><strong>Reconocimiento</strong></em>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;Se incluye informaci&oacute;n de citaci&oacute;n?</p>
                <p>P.ej. Informaci&oacute;n de como citar tu software como citaci&oacute;n.</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                </ul>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;los diferentes lanzamientos tienen un identificador global &uacute;nico persistente (PID)?
                    (p.ej. DOI de ZENODO)</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                </ul>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;la informaci&oacute;n de citaci&oacute;n incluye el ORCID de, al menos uno, los autores?</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>NO</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="297">
                <p>&iquest;el software est&aacute; registrado en un registro especifico de su dominio?</p>
                <p>En caso afirmativo, por favor, liste los registros correspondientes (nombre y URL de cada uno).</p>
            </td>
            <td colspan="3" width="297">
                <ul>
                    <li>SI</li>
                    <li>Nombre y URL</li>
                    <li>NO</li>
                    <li>N/A</li>
                </ul>
                <p>&nbsp;</p>
            </td>
        </tr>
    </tbody>
</table>



Los desarrolladores de cada uno de los componentes descritos en la Implementación de Referencia deberán indicar en cual de las 4 etapas de desarrollo se encuentra su componente. A continuación completar el checklist del SMP de ELIXIR, indicando la respuesta a cada uno de los parámetros descritos. Esto permitirá evaluar de formar sistmática y metódica cada componente, indicando si para la fase de desarrollo indicada, el componente cumple con todos los parámetros esperados.

## Software Management Wizard

Para facilitar la tarea a los desarrolladores y sistematizar al máximo el proceso de evaluación se usará el *"Software Management Wizard"* de Elixir.

```{important}

Enlace al **Software Management Wizard** de Elixir: [Software Management Wizard](https://smw.dsw.elixir-europe.org/wizard/?originalUrl=%2Fwizard%2Fdashboard).
```

[SMP componentes](../../files/20241220_anexo_analisis_smp_componentes_implementacion_referencia_IMPaCT-Data_p.pdf)

## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>





