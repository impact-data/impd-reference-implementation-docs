# Evaluación externa - Experiencia del usuario

```{important}

La **evaluación** de los componentes de la Implementación de Referencia de IMPaCT-Data por parte de los usuarios se ha ampliado hasta el **31 de Marzo de 2025**

Muchas gracias por vuestra colaboración.

```

Para evaluar la **Experiencia del Usuario** de cada uno de los componentes de la Implementación de Referencia se usará la **System Usability Scale (SUS)** junto a algunas preguntas técnicas. 

## Escala de Usabilidad (SUS)

Enlace al documento de referencia: [System Usability Scale (SUS)](https://www.researchgate.net/publication/228593520_SUS_A_quick_and_dirty_usability_scale).

La idea es verificar con los parámetros definidos en la SUS si los componentes son “apropiados para su objetivo previsto” y para ello solicitaremos a varios usuarios finales que instalen, ejecuten y prueben los diferentes componentes y los evalúen pasando la escala SUS.

La escala SUS es una escala simple, de 10 items que ofrece una visión global de evaluaciones subjetivas sobre la usabilidad. 

| _**Concepto**_                                                                     | _**Valoración (1 Completamente en desacuerdo - 5 Completamente de acuerdo**_ |
| ---------------------------------------------------------------------------------- | ---------------------------------------------------------------------------- |
| 1\. Creo que me gustaría utilizar este sistema con frecuencia.                     | 1 - 5 | 
| 2\. Creo que el sistema es innecesariamente complejo.                              | 1 - 5 |
| 3\. Creo que el sistema era fácil de usar.                                         | 1 - 5 |
| 4\. Creo que necesitaría el apoyo de un técnico para poder utilizar este sistema   | 1 - 5 |
| 5\. Creo que las diversas funciones de este sistema están bien integradas          | 1 - 5 |
| 6\. Creo que el sistema tiene demasiadas inconsistencias.                          | 1 - 5 |
| 7\. Creo que la mayoría de gente podría aprender a usar este sistema rápidamente.  | 1 - 5 |
| 8\. El sistema me pareció muy complejo de usar                                     | 1 - 5 |
| 9\. Me sentí seguro al usar usar el sistema.                                       | 1 - 5 |
| 10\. He necesitado aprender muchas cosas antes de poder empezar a usar el sistema. | 1 - 5 |

## Usando la escala SUS

La escala SUS generalmente se utiliza después de que el encuestado haya tenido la oportunidad de utilizar el sistema que se está evaluando, pero antes de que se lleve a cabo cualquier sesión informativa o discusión. Se debe pedir a los encuestados que registren su respuesta inmediata a cada ítem, en lugar de pensar en los ítems durante mucho tiempo (es una escala subjetiva).  Todos los elementos deben ser revisados. Si un encuestado siente que no puede responder a un ítem en particular, debe marcar el punto central de la escala.

## Formulario de Evaluación de los Usuarios


```{important}

Enlace al formulario:  [**Formulario de Evaluación de los usuarions**](https://form.typeform.com/to/w4sXetnm).

Cada componente se evalúa por separado, por lo que debe completarse el formulario una vez por cada componente que se pruebe.
```

## Puntuando la escala SUS

La escala SU produce un número único que representa una medida compuesta de la usabilidad general del sistema que se está estudiando. Es importante destacar que las puntuaciones de elementos individuales no son significativas por sí solas.  

Para calcular la puntuación de la escala SUS se deben sumar las contribuciones de cada ítem. La contribución a la puntuación total de cada ítem variará de 0 a 4. 
- Para los ítems 1,3,5,7 y 9, la contribución a la puntuación es la posición de la escala menos 1. 
- Para los ítems 2,4,6,8 y 10, la contribución es 5 menos la posición de la escala. 

Multiplique la suma de las puntuaciones por 2,5 para obtener el valor total del componente en la escala SUS.  

Las puntuaciones SUS tienen un rango de 0 a 100. Siendo 100 un sistema muy “usable” según el criterio subjetivo de los usuarios.

## Preguntas técnicas

El formulario de evaluación de los usuarios incluye varias preguntas técnicas para recabar más información sobre los componentes:

- Claridad de las descripciones
- Facilidad de la instalación
- Facilidad de ejecución
- Capacidad de hacer las pruebas
- Seguridad
- Interoperabilidad


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>

