# Beacon Network

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

ELIXIR Beacon Network (EBN) tiene como objetivo actuar como un recurso para facilitar la capacidad de descubrimiento de datos al permitir a los usuarios buscar en múltiples Beacons simultáneamente en una sola consulta. Creada en el marco de ELIXIR, esta red establece un ecosistema interconectado de balizas, fomentando el intercambio responsable de tipos de datos genómicos, fenotípicos y de otro tipo.

Cuando varias instituciones u organizaciones implementan un Beacon y lo hacen accesible a una red, forman una Beacon Network. Esta red permite consultas federadas a través de múltiples balizas sin la necesidad de almacenamiento de datos centralizado o acceso directo a los datos.

:::{figure} ../../../images/beacon_network_schema.png
:align: center

*Beacon v2 Networks*
:::

La red tiene como objetivo aumentar la visibilidad y accesibilidad de los datos genómicos y fenoclínicos para facilitar el intercambio de datos y la colaboración entre investigadores. Al proporcionar una forma uniforme de consultar datos genómicos de múltiples fuentes, Beacon Network ayuda a superar los desafíos de la fragmentación y la heterogeneidad de los datos en las ciencias biológicas y permite nuevas investigaciones gracias a sus descubrimientos.

En el marco del proceso de desarrollo de IMPaCT-Data se han generado un [piloto Beacon Network](https://impact-beacon-network-demo.ega-archive.org/) con diferentes instituciones asociadas al proyecto como se muestra en la siguiente tabla:

:::{figure} ../../../images/beacon_network_pilot_participating_centers.png
:align: center

:::

Se pueden encontrar más detalles en el siguiente [documento sobre la comparativa de procesos en entornos hospitalarios](https://b2drop.bsc.es/index.php/apps/files/?dir=/IMPaCT-Data/Entregables&openfile=3259793).

De la misma forma, se puede ver una versión extendida de este component en el [Informe final sobre el funcionamiento de la red de IMPaCT de Beacons](https://b2drop.bsc.es/index.php/apps/files/?dir=/IMPaCT-Data/Entregables&openfile=3248197).

:::{figure} ../../../images/beacon_network_geographic_distribution.png
:align: center

:::


## Documentación técnica

### Detalles técnicos

+ [Informe final sobre el funcionamiento de la red IMPaCT beacons](https://b2drop.bsc.es/index.php/f/3248197)
+ [Documentación oficial para desplegar un Beacon v2 Network](https://beacon-network-v2-documentation.readthedocs.io/en/latest/light-beacon-network/)

### Documentación técnica

[https://github.com/elixir-europe/beacon-network-docker/blob/master/README.md](https://github.com/elixir-europe/beacon-network-docker/blob/master/README.md)

### Repositorio de código

[https://github.com/elixir-europe/beacon-network-docker](https://github.com/elixir-europe/beacon-network-docker)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>