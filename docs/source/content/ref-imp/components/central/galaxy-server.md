# Servidor Galaxy

## Descripción del componente

### Galaxy

[Galaxy](https://galaxyproject.org/) es una plataforma web ampliamente utilizada para realizar investigaciones de índole bioinformátiico teniendo como principal objetivo que estas sean accesibles, reproducibles y transparentes. Galaxy proporciona una interfaz fácil de usar para que los investigadores puedan ejecutar una amplia gama de tareas y flujos sin necesidad de tengan conocimientos avanzados de programación.

El proyecto UseGalaxy cuenta con el respaldo de una gran cantidad de servidores a lo largo de todo el mundo. Los principales servidores públicos de incluyen:

- [Galaxy Main](https://usegalaxy.org/): Instancia principal alojada en Penn State University ([usegalaxy.org](https://usegalaxy.org/)).

- [Galaxy Europe](https://usegalaxy.eu/): Instancia europea alojada en Friburgo, Alemania ([usegalaxy.eu](https://usegalaxy.eu/)).

- [Galaxy Australia](https://usegalaxy.org.au/): Instancia australiana alojada en Melbourne ([usegalaxy.org.au](https://usegalaxy.org.au/)).

Además, hay muchas otras instancias de Galaxy alojadas por instituciones y grupos de investigación a nivel mundial, lo que contribuye a un rico ecosistema de servidores Galaxy.

Dentro de IMPaCT-Data se ha desarrollado un ecosistema Galaxy compuesto de un nodo central ([https://galaxy.impact-data.bsc.es/](https://galaxy.impact-data.bsc.es/)) y una serie de nodos externos de trabajo Pulsar.

## Documentación técnica

### Instalación de Galaxy

La implementación de la instancia UseGalaxy para IMPaCT-Data se realiza a través de Ansible. Esto implica automatizar el proceso de instalación y configuración para garantizar la coherencia y la repetibilidad.

Aunque IMPaCT-Data WP2 proporciona unas [recetas de implementación](https://gitlab.bsc.es/impact-data/galaxy-server), es importante tener en cuenta que los requisitos para una implementación exitosa de UseGalaxy pueden variar de un nodo a otro. Para ir paso a paso a través del proceso de un servidor UseGalaxy, los siguientes tutoriales prácticos oficiales:

 + [Tutorial oficial](https://training.galaxyproject.org/training-material/topics/admin/tutorials/ansible-galaxy/tutorial.html)

 ### Repositorio de código

 + [https://gitlab.bsc.es/impact-data/galaxy-server](https://gitlab.bsc.es/impact-data/galaxy-server)


 ## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>