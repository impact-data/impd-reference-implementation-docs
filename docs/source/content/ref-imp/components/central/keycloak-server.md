# Servidor de Autenticación/Autorización (Keycloak)

## Descripción del componente

Como el servidor cenral de la auteticacion/autorización el proyecto utiliza el servidor de identidad [Keycloak](https://www.keycloak.org/). 
Keycloak is an opensource [OpenID Connect 1.0](https://openid.net/specs/openid-connect-core-1_0.html) Identity Provider Server initially developed as a [WildFly](https://www.wildfly.org/) Enterprise Java Server subproject and currenly is a separate project supported by [Linux Foundation](https://www.linuxfoundation.org). Althouth for more than ten years Keycloak was based on the WildFly, latest versions are implemented using [Quarkus](https://quarkus.io) microservices platform.
En IMPaCT-Data cada participante mantiene su propia instalacion del proveedor de las identidades compatible con OpenID Connect que funcionan como proveedores externos para el servidor central.

## Agregaciones de GA4GH Passport Visas

IMPaCT-Data sugiere el uso de los "visados" de GA4GH Passport para la autorización de acceso a los datos sensibles. Dado que la autorización se otorga por cada participante que es el propietario de sus datos, el servidor central agrega los permisos de lo proveedores externos. Esa informacion no es usable para la autorización sino se puede usar para seleccionar los recursos accesibles para el usuario. Para el acceso al recurso protegido es necesario obtener el token del servidor autorizante.
Actualmente Keycloak no soporta la agregacion de los scopes dinamica, por eso hemos desarrollado un plug-in que realiza esa funciolidad - [external-idp-scopes-protocol-mapper](https://gitlab.bsc.es/inb/keycloak/external-idp-scopes-protocol-mapper).

## Documentación técnica

### Instalación de Keycloak

Aunque los socios de IMPaCT pueden instalar un servidor Keycloak desde el [sitio oficial del proyecto](https://www.keycloak.org/) o utilizar una instancia de Keycloak ya existente, proporcionamos una [plantilla simple de docker compose](https://gitlab.bsc.es/impact-data/impd-keycloak-docker) que facilita la instalación y viene con una configuración de _realms_ ya preconfigurada.

```bash
>git clone https://gitlab.bsc.es/impact-data/impd-keycloak-docker.git
>cd impd-keycloak-docker
```

**DEBES** cambiar la variable **KEYCLOAK_ADMIN_PASSWORD** en el fichero `docker-compose.yaml` por una contraseña segura, ya que esta contraseña concede acceso de administrador a toda la instalación de Keycloak!

```bash
>docker-compose up -d
```

Esto debería ejecutar una instancia preconfigurada del servidor Keycloak de IMPaCT-Data en el puerto **8686**. Tenga en cuenta que el _realm_ preconfigurado de IMPaCT-Data proporcionado no contiene secretos de cliente (se establecieron por defecto en '1234567890'), por lo que es necesario añadir seguridad a los clientes de IMPaCT-Data regenerando los secretos.

:::{figure} ../../../images/kc-regenerate-secret.png
:align: center

*Regenerating 'galaxy.impact-data.bsc.es' secret key*
:::

Las claves secretas deben ser regeneradas para todos los clientes de IMPaCT-Data:

- galaxy.impact-data.bsc.es
- impact-data-rest
- impact-data-portal
- bsc-impact-nextcloud

Las plantillas de configuración incluyen la configuración del proveedor externo de Life Science AAI, que también necesita un identificador de cliente y un secreto correctos. Estas credenciales son proporcionadas por el servidor LS Login a través del proceso de registro descrito [aquí](https://docs.google.com/document/d/17pNXM_psYOP5rWF302ObAJACsfYnEWhjvxAHzcjvfIE/edit).

### ¿Cómo Agregar Proveedores Externos?

La idea principal detrás de la seguridad de IMPaCT-Data es que cada socio mantiene su propio servidor de identidad OIDC que se utiliza como un Proveedor de Identidad externo para IMPaCT-Data. La configuración general se describe en la [documentación](https://www.keycloak.org/docs/latest/server_admin/index.html#_identity_broker_oidc) de Keycloak. Sin embargo, se requiere una configuración específica.
Vamos a crear el proveedor _"My Gorgeous Institution"_.

:::{figure} ../../../images/kc-add-external-provider.png
:align: center

*Adding External Identity Provider*
::: 

Elige el tipo de proveedor "OpenID Connect v1.0" y completa el formulario con los parámetros obligatorios:
* **Alias** - el nombre corto para el proveedor (por ejemplo, "mgi")
* **Client ID** - el identificador del cliente IMPaCT-Data en el servidor de identidad de la institución.
* **Client Secret** - el secreto del cliente IMPaCT-Data en el servidor de identidad de la institución.
* **Authorization URL** - el _endpoint_ de autorización de la institución.
* **Token URL** - el _token endpoint_ de la institución.  
  Si el servidor de identidad de la institución soporta OpenID Connect Discovery, se puede proporcionar el _discovery endpoint_ (url que termina con "/.well-known/openid-configuration") en lugar de las últimas dos URLs.  
  En la parte "Avanzada" de la configuración, proporciona 
* **Scopes** - "offline_access ga4gh_passport_v1"  
  El _scope_ "__offline_access__" solicita el __token offline__ para que IMPaCT-Data pueda solicitar tokens "mgi" indefinidamente.  
  El _scope_ "__ga4gh_passport_v1__" es necesario por razones de seguridad - no podremos solicitar un token de acceso con _scopes_ no definidos en el _refresh token_.
* **Store tokens** - "ON"
* **Stored tokens readable** - "ON"

__Añadiendo el nuevo Proveedor de Identidad Externa en la página de Inicio de Sesión__  

  Keycloak permite la personalización de las páginas de usuario utilizando plantillas FreeMarker ([doc](https://www.keycloak.org/docs/latest/server_development/#_themes)). IMPaCT-Data utiliza un tema de inicio de sesión personalizado ubicado en `/impd-keycloak-docker/keycloak/themes/impact-data/login/login.ftl`.  
Para agregar el nuevo botón de inicio de sesión, simplemente agrega otro caso:

```html
<#case "ls-login">
  <span class="${properties.kcFormSocialAccountNameClass!}">
    <img src="${url.resourcesPath}/img/ls-login.svg" style="width:100px;display:block;margin:0 auto" alt="elixir"/>
  </span>
  <#break>
<#case "mgi">
  <span class="${properties.kcFormSocialAccountNameClass!}">
    MGI
  </span>
  <#break>
```
</small>

### Configuración del intercambio de _tokens_

Dado que cada socio de IMPaCT-Data gestiona su propio servidor de Proveedor de Identidad, los recursos protegidos solo reconocerían _tokens_ emitidos por el IDP institucional. Por otro lado, los flujos de trabajo de IMPaCT-Data están provistos del _token_ de IMPaCT-Data, por lo que para acceder a los recursos protegidos, el _token_ de IMPaCT-Data debe ser _intercambiado_ por el _token_ de confianza del proveedor de datos. Para esta función de intercambio de tokens, se debe configurar [RFC 8693](https://datatracker.ietf.org/doc/html/rfc8693) para el ámbito de IMPaCT-Data.

Para habilitar el _Intercambio de Tokens_ para el IDP externo, se deben habilitar los permisos:

:::{figure} ../../../images/kc-enable-token-exchange.png
:align: center

*Enable Token Exchange permissions*
:::

Luego, elija los permisos "token-exchange". En los detalles de los permisos, agregue "política de intercambio de tokens", marque "Afirmativo" y "Guardar".

:::{figure} ../../../images/kc-token-exchange-policy.png
:align: center

*Apply Token Exchange Policy*
:::

Tenga en cuenta que la "política de intercambio de tokens" ya estaba preconfigurada para el cliente "impact-data-rest".

Ahora el token de IMPaCT-Data ("subject_token=$KT") puede ser intercambiado por el externo ("requested_issuer=mgi"):
```bash
curl -X POST -d "client_id=impact-data-rest" -d "requested_issuer=mgi" \
--data-urlencode "grant_type=urn:ietf:params:oauth:grant-type:token-exchange" \ 
--data-urlencode "requested_token_type=urn:ietf:params:oauth:token-type:access_token" \
-d "subject_token=$KT" \
https://inb.bsc.es/auth/realms/impact-data/protocol/openid-connect/token
```

### Agregación de Visas GA4GH

Antes de intentar acceder a los datos protegidos, es razonable saber qué datos son accesibles para el usuario. La distribución de Keycloak de IMPaCT-Data se ha ampliado con un _mapeador de scope_ personalizado `/impd-keycloak-docker/keycloak/providers/external-idp-scopes-protocol-mapper-0.0.1.jar` que consulta dinámicamente a proveedores externos para obtener _scopes_. La instalación ya incluye el _scope_ "ga4gh_passport_v1" sin configurar para agregar Visas GA4GH de proveedores externos.
Necesitamos mapear los _scopes_ "ga4gh_passport_v1" de nuestros proveedores externos en el token de IMPaCT-Data:

:::{figure} ../../../images/kc-configure-mapper.png
:align: center

*Keycloak mapper configuration*
:::

Elija _External IDP Claim Mapper_

:::{figure} ../../../images/kc-external-mapper.png
:align: center

*Keycloak external mapper*
:::


* **Name** - cualquier nombre razonable (por ejemplo, "mgi ga4gh_passport_v1 mapper")
* **External IDP** - Alias del IDP externo
* **External claim name** - ga4gh_passport_v1 (nombre de la reclamación de Pasaporte GA4GH)
* **Token claim name** - ga4gh_passport_v1 (nombre de la reclamación de Pasaporte GA4GH)

### Autenticación con el servidor de identidad IMPaCT-Data

Debido a que el servidor de identidad IMPaCT-Data es un proveedor de identidad estándar OpenID Connect 1.0, se pueden utilizar cualquier cliente compatible con él. Se pueden encontrar algunas recetas en la [documentación](https://www.keycloak.org/docs/25.0.2/securing_apps/#openid-connect) de Keycloak.

En la página referente a la [configuración local de clientes para Keycloak](https://impact-data-ref-imp.readthedocs.io/es/latest/content/ref-imp/components/local/keycloak-client.html) aparecen diferentes ejemplos de autenticación con el servidor de identidad de IMPaCT-Data


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>