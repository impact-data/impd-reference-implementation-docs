# Guía de referencia para la adecuación de IMPaCT-Data al Esquema Nacional de Seguridad (ENS)

## Descripción del componente
El [Real Decreto 311/2022, de 3 de mayo, por el que se regula el Esquema Nacional de Seguridad de España (ENS)](https://www.boe.es/buscar/doc.php?id=BOE-A-2022-7191), establece un conjunto de 73 medidas de seguridad agrupadas en 3 marcos: (i) marco organizativo, (ii) marco operacional y (iii) marco de protección. Dentro de cada uno de ellos se establecen varios niveles tal como se muestra en la Figura 1 “Organización de Medidas del Esquema Nacional de Seguridad”.

:::{figure} ../../../images/organizacion_medidas_ens_2024.png
:align: center

*Figura 1: Organización de Medidas del Esquema Nacional de Seguridad*
:::

**En el marco de IMPaCT-Data**, se ha desarrollado mediante la recopilación de la experiencia de los socios participantes, una **Guía de Buenas Prácticas en el Manejo de Datos Reales en base al Esquema Nacional de Seguridad de España** y siguiendo como metodología la **realización de una serie de encuestas sobre una selección de medidas de seguridad** del ENS a los diferentes nodos definidos de la infraestructura, concretamente los nodos definidos en el [entregable 2.2 "Diseño Inicial de la Arquitectura de la Infraestructura”](https://b2drop.bsc.es/index.php/apps/files/?dir=/IMPaCT-Data/Entregables&openfile=2152240): (i) nodo central, (ii) nodo proveedor de datos sensibles, nodo proveedor de computación y (iii) nodo que actúa como proveedor de datos sensibles y como proveedor de computación.

**Las encuestas** realizadas fueron enviadas a **todos los nodos de IMPaCT-Data**; sin embargo, y debido a la alta complejidad para la completitud de la misma y su extensión, de la totalidad de los 45 nodos de IMPaCT-Data se obtuvieron las siguientes **7 respuestas**: Hospital Clínic de Barcelona y Hospital Universitario Virgen del Rocío como **nodos proveedores de datos sensibles**, Instituto de Biomedicina de Sevilla y NavarraBioMed de Navarra como **nodos proveedores de computación** e IISLAFE de Valencia, UMIB- Fisabio de Valencia y NASERTIC de Navarra como **nodos proveedores de datos sensibles y de computación**.

:::{figure} ../../../images/mapa_instituciones_respuesta_encuesta_ens_2024.png
:align: center
:width: 50%

*Figura 2. Mapa con las instituciones que han respondido a la encuesta sobre medidas de seguridad del ENS de España realizada a los Nodos de IMPaCT-Data [(en gris) instituciones participantes en IMPaCT-Data, (en azul) instituciones participantes en IMPaCT-Data que respondieron a la encuesta]*
:::

<br/>

:::{figure} ../../../images/diagrama_nodos_participantes_encuesta_ens_2024.png
:align: center
:width: 75%

*Figura 3. Diagrama de nodos participantes en IMPaCT-Data que respondieron a la encuesta [Ilustración: Rodríguez-Mejías S, Degli-Esposti S, González-García S, Parra-Calderón CL. Toward the European Health Data Space: The IMPaCT-Data secure infrastructure for EHR-based precision medicine research. J Biomed Inform. Published online June 14, 2024. doi:10.1016/j.jbi.2024.104670]*
:::

Tras la realización de estas encuestas, *se identificaron un conjunto de Buenas Prácticas y Recomendaciones en el Manejo de Datos Reales* para su uso y aplicación en IMPaCT-Data. Adicionalmente y a partir de la experiencia previa de los socios de IMPaCT-Data que ha sido recopilada, en el E6.5 se definen: (i) recomendaciones por tipología de nodo de la Infraestructura y (ii) recomendaciones siguiendo aspectos relacionados con las medidas de aplicación.

:::{figure} ../../../images/buenas_practicas_manejo_uso_datos_reales.png
:align: center

*Figura 4. Buenas Prácticas en el Manejo de Datos Sensibles en cumplimiento con el Esquema Nacional de Seguridad en IMPaCT-Data*
:::

Este conjunto de Buenas Prácticas y Recomendaciones busca establecer **directrices claras y prácticas que permitan asegurar la seguridad, integridad y confidencialidad de los datos sensibles manejados en el marco de la infraestructura IMPaCT-Data**. El cumplimiento de las mismas no solo es esencial para garantizar la protección de los datos sensibles, sino que también contribuye a cumplir con las normativas y regulaciones de seguridad de datos vigentes en España. Además, fortalece la confiabilidad de la infraestructura IMPaCT-Data, promoviendo la confianza de los usuarios y partes interesadas en la gestión de los datos en cuestión​.

Esta Guía de Buenas Prácticas ha sido obtenida a partir del trabajo realizado en el [entregable 6.5 “Guía de Buenas Prácticas en el Manejo de Datos Reales”](https://b2drop.bsc.es/index.php/apps/onlyoffice/3165210?filePath=%2FIMPaCT-Data%2FEntregables%2FEn%20Proceso%2FM40%20-%20Abril%202024%2FE6.5.%20Gu%C3%ADa%20de%20Buenas%20Pr%C3%A1cticas%20en%20el%20Manejo%20de%20Datos%20Reales%20(M36%2C%20v.1.6)%20-%20VERSI%C3%93N%20FINAL.docx). En el mismo, se han integrado y revisado **conceptos esenciales** como “Seguridad desde el diseño y por defecto”, el “Esquema Nacional de Seguridad de España”, el “Reglamento General de Protección de Datos” y la “Ley Orgánica de Protección de Datos y Garantía de los Derechos Digitales”. Estos conceptos forman la base de las Buenas Prácticas y aseguran el cumplimiento regulatorio y la protección de datos personales. Su comprensión y aplicación son fundamentales para la implementación de un marco seguro y eficiente en el manejo de datos sensibles.

Se ha destacado la **importancia de las ciberamenazas** a las que se enfrentan los proveedores de datos sanitarios. Esto incluye la identificación y mitigación de riesgos asociados con el manejo de datos sensibles, asegurando que las medidas de seguridad sean suficientemente robustas para prevenir brechas de seguridad y fugas de datos. La creciente frecuencia y sofisticación de los ataques cibernéticos subraya la necesidad de una vigilancia constante y la actualización de las estrategias de defensa.

Se ha abordado la **relación entre el Espacio Europeo de Datos de Salud e IMPaCT-Data**, enfatizando el reciente Reglamento sobre el Espacio Europeo de Datos Sanitarios. Este reglamento proporciona un marco regulatorio que facilita el intercambio seguro y eficiente de datos sanitarios a nivel europeo. La alineación con estos estándares europeos es crucial para garantizar la interoperabilidad y la colaboración transfronteriza en la investigación biomédica.

Se ha presentado una **aproximación teórica del ciclo de vida de los datos según su tipología** (datos de imagen médica, datos genómicos y datos clínicos). Esto incluye aspectos cruciales para el uso local y federado de datos, garantizando que todas las etapas del ciclo de vida se realicen conforme a las mejores prácticas de seguridad. Este enfoque asegura que los datos se manejen de manera segura desde su adquisición hasta su eventual eliminación.

**Se ha realizado una encuesta a los nodos de IMPaCT-Data para evaluar la viabilidad o dificultad de implementación de una selección de medidas de seguridad del Esquema Nacional de Seguridad**. Los resultados de esta encuesta han sido fundamentales para identificar las medidas de seguridad viables y aquellas que presentan desafíos, permitiendo la elaboración de recomendaciones específicas. Este proceso participativo ha sido clave para adaptar las recomendaciones a las realidades y capacidades de los diferentes nodos.

**Se ha definido un conjunto de Buenas Prácticas aplicables y una serie de Recomendaciones para el manejo de datos reales y sensibles en investigación biomédica**. Estas recomendaciones incluyen aspectos relacionados con el análisis de riesgos, la anonimización y seudonimización, la accesibilidad y trazabilidad, y la descubribilidad preservando la privacidad. La adopción de estas prácticas es esencial para proteger la integridad y confidencialidad de los datos y para fomentar una cultura de seguridad y responsabilidad en la gestión de la información.

Por tanto, el E6.5 se presenta como una continuación del [entregable 6.4 “Aspectos de Seguridad en el Manejo de Datos Sensibles”](https://b2drop.bsc.es/index.php/apps/files/?dir=/IMPaCT-Data/Entregables&openfile=2772869), donde se revisaron aspectos legales, éticos, organizativos y técnicos. Además, complementa el [entregable 2.2 “Diseño Inicial de la Arquitectura de la Infraestructura”](https://b2drop.bsc.es/index.php/apps/files/?dir=/IMPaCT-Data/Entregables&fileid=412591) proporcionando directrices prácticas para la implementación de medidas de seguridad en los nodos de la infraestructura IMPaCT-Data. Esta continuidad y complementación aseguran un enfoque integral y cohesivo en la gestión de datos sensibles dentro del proyecto. La implementación de estas Buenas Prácticas y Recomendaciones no solo garantiza la seguridad y privacidad de los datos sensibles, sino que también promueve la colaboración científica y el avance conjunto en la investigación biomédica y contribuye a cumplir con las normativas y regulaciones vigentes siendo esencial para cumplir con las normativas y regulaciones vigentes, y fortalecer la confianza de los proveedores de datos del Sistema Nacional de Salud en la infraestructura IMPaCT-Data y su siguiente iteración.

[Real Decreto 311/2022, de 3 de mayo, por el que se regula el Esquema Nacional de Seguridad y en el que se encuentran de forma detallada la completitud de las 72 medidas de seguridad en las que se ha basado este estudio junto con sus refuerzos correspondientes](https://www.boe.es/buscar/doc.php?id=BOE-A-2022-7191)

[Entregable 2.2 de IMPaCT-Data “Diseño Inicial de la Arquitectura de la Infraestructura”](https://b2drop.bsc.es/index.php/apps/files/?dir=/IMPaCT-Data/Entregables&fileid=412591)

[Entregable 6.4 de IMPaCT-Data “Aspectos de seguridad en el manejo de Datos Sensibles”](https://b2drop.bsc.es/index.php/apps/files/?dir=/IMPaCT-Data/Entregables&fileid=412591)

[Entregable 6.5 de IMPaCT-Data “Guía de Buenas Prácticas en el Manejo de Datos Reales” en el que se detalla la metodología seguida para la realización de las encuestas sobre las medidas de seguridad del ENS y la identificación de las buenas prácticas para el manejo de datos reales](https://b2drop.bsc.es/index.php/apps/onlyoffice/3165210?filePath=%2FIMPaCT-Data%2FEntregables%2FEn%20Proceso%2FM40%20-%20Abril%202024%2FE6.5.%20Gu%C3%ADa%20de%20Buenas%20Pr%C3%A1cticas%20en%20el%20Manejo%20de%20Datos%20Reales%20(M36%2C%20v.1.6)%20-%20VERSI%C3%93N%20FINAL.docx)

[Rodríguez-Mejías S, Degli-Esposti S, González-García S, Parra-Calderón CL. Toward the European Health Data Space: The IMPaCT-Data secure infrastructure for EHR-based precision medicine research. J Biomed Inform. Published online June 14, 2024. doi:10.1016/j.jbi.2024.104670](https://www.sciencedirect.com/science/article/pii/S1532046424000881)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>