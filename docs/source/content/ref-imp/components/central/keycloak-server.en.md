# Autenticación/Autorización Server (Keycloak)

## Component Description
The [Keycloak](https://www.keycloak.org/) Authentication server is used as the central Authentication/Authorization server of the project.
Keycloak is an opensource [OpenID Connect 1.0](https://openid.net/specs/openid-connect-core-1_0.html) Identity Provider Server initially developed as a [WildFly](https://www.wildfly.org/) Enterprise Java Server subproject and currenly is a separate project supported by [Linux Foundation](https://www.linuxfoundation.org). Althouth for more than ten years Keycloak was based on the WildFly, latest versions are implemented using [Quarkus](https://quarkus.io) microservices platform.
En IMPaCT-Data cada participante mantiene su propia instalacion del proveedor de las identidades compatible con OpenID Connect que funcionan como proveedores externos para el servidor central.

## Agregaciones de GA4GH Passport Visas

IMPaCT-Data recommends an adoption of the GA4GH Passport "visas" as an authorization method for sensitive data access. Given that  autorización is provided by each participant who possess the ownership of the protected data, the central server aggregates permissions provided by the external parties. This information is not supposed to be used for the authorization means, but rather for the listing of protected resources available for the user. To access protected resources it is needed to obtain the access token from the corresponding authorization server.
At the moment, Keycloak does not support a dynamic aggregation of externally provided scopes, so special plug-in has been developed - [external-idp-scopes-protocol-mapper](https://gitlab.bsc.es/inb/keycloak/external-idp-scopes-protocol-mapper).

## Technical Documentation

### Keycloak Installation

Although IMPaCT delployers may install the Keycloak server from the [project site](https://www.keycloak.org/) or use already existing Keycloak instance, we provide a simple [docker compose template](https://gitlab.bsc.es/impact-data/impd-keycloak-docker) that facilitates the installation and goes with already preconfigured realm configuration.

```bash
>git clone https://gitlab.bsc.es/impact-data/impd-keycloak-docker.git
>cd impd-keycloak-docker
```
You **MUST** change the **KEYCLOAK_ADMIN_PASSWORD** in the `docker-compose.yaml` to the secret one, as this gives a total, administrative access to the Keycloak installation!

```bash
>docker-compose up -d
```
This should run a preconfigured instace of the IMPaCT-Data Keycloak server at the port **8686**.
Note, that provided preconfigured IMPaCT-Data realm contains no client secrets (they defaulted to the '1234567890'), thus there is a need to secure IMPaCT-Data clients by regenerating the secrets.

![](docs/source/content/images/kc-regenerate-secret.png) _Regenerating 'galaxy.impact-data.bsc.es' secret key_

The secret keys must be regenerated for all IMPaCT-Data clients:

- galaxy.impact-data.bsc.es
- impact-data-rest
- impact-data-portal
- bsc-impact-nextcloud

The configuration templates also goes with Life Science AAI external provider configuration which also needs a correct client identifier and secret. These credentials are provided by the LS Login server via registration process described [here](https://docs.google.com/document/d/17pNXM_psYOP5rWF302ObAJACsfYnEWhjvxAHzcjvfIE/edit).

### How to Add External Providers

The principal idea behind the IMPaCT-Data security is that each partner keeps its own OIDC identity server that is used as an external Identity Provider for the IMPaCT-Data. The general configuration is described in the Keycloak [documentation](https://www.keycloak.org/docs/latest/server_admin/index.html#_identity_broker_oidc). Nevertheless, some specific configuration is required.
Let's create the "My Gorgeous Institution" provider.

![](docs/source/content/images/kc-add-external-provider.png)
_Adding External Identity Provider_ 

Choose "OpenID Connect v1.0" type of provider and fill the form with mandatory parameters:
* **Alias** - the short name for the provider (e.g. "mgi")
* **Client ID** - the clien_id of the IMPaCT-Data client in the institution's identity server.
* **Client Secret** - the client_secret of the IMPaCT-Data client in the institution's identity server.
* **Authorization URL** - the institution's authorization endpoint.
* **Token URL** - the institution's token endpoint.  
  If the institution's identity server supports OpenID Connect Discovery, the discovery endpoint (url ended with "/.well-known/openid-configuration") may be provided instead of the last two URLs.  
  In the "Advanced" part of the configuration, provide 
* **Scopes** - "offline_access ga4gh_passport_v1"  
  "__offline_access__" scope asks for the __offline token__ so IMPaCT-Data may request "mgi" tokens endlessly.  
  "__ga4gh_passport_v1__" scope is needed for security reasons - we won't be able to request an access token with scopes undefined in the refresh token.
* **Store tokens** - "ON"
* **Stored tokens readable** - "ON"

__Adding the new External Identity Provider into the Login page__  

  Keycloak allows customization of user pages using FreeMarker templates ([doc](https://www.keycloak.org/docs/latest/server_development/#_themes)). IMPaCT-Data uses customized login theme located at `/impd-keycloak-docker/keycloak/themes/impact-data/login/login.ftl`.  
To add the new login button just add another case:

```html
<#case "ls-login">
  <span class="${properties.kcFormSocialAccountNameClass!}">
    <img src="${url.resourcesPath}/img/ls-login.svg" style="width:100px;display:block;margin:0 auto" alt="elixir"/>
  </span>
  <#break>
<#case "mgi">
  <span class="${properties.kcFormSocialAccountNameClass!}">
    MGI
  </span>
  <#break>
```
</small>

### Token Exchange configuration

Since each partner of IMPaCT-Data governs their own Identy Provider server, protected resources would only recognize tokens issued by
institutional IDP. On the other hand, IMPaCT-Data workflows are provided with the IMPaCT-Data token, thus to access protected resources IMPaCT-Data token must be _exchanged_ for the token trusted by the data provider. For this Token Exchange feature [RFC 8693](https://datatracker.ietf.org/doc/html/rfc8693) must be configured for the IMPaCT-Data realm.

To enable Token Exchange for the External IDP Permissions must be enabled:
![](docs/source/content/images/kc-enable-token-exchange.png)
_Enable Token Exchange permissions_

Then choose "token-exchange" permissions. In the permissions details, add "token exchange policy", check "Affirmative" and "Save".

![](docs/source/content/images/kc-token-exchange-policy.png)
_Apply Token Exchange Policy_

Note that the "token exchange policy" was already preconfigured for the "impact-data-rest" client.

Now the IMPaCT-Data token ("subject_token=$KT") may be exchanged to the external one ("requested_issuer=mgi"):
```bash
curl -X POST -d "client_id=impact-data-rest" -d "requested_issuer=mgi" \
--data-urlencode "grant_type=urn:ietf:params:oauth:grant-type:token-exchange" \ 
--data-urlencode "requested_token_type=urn:ietf:params:oauth:token-type:access_token" \
-d "subject_token=$KT" \
https://inb.bsc.es/auth/realms/impact-data/protocol/openid-connect/token
```
### GA4GH Visas Aggregation

Before trying to get an access to the protected data it is reasonable to know what data is accessble for the user.
IMPaCT-Data Keycloak distribution is extended with a custom scope mapper `/impd-keycloak-docker/keycloak/providers/external-idp-scopes-protocol-mapper-0.0.1.jar` that dynamically query external providers for scopes.
The instalation already includes uncofigured "ga4gh_passport_v1" scope to aggregate GA4GH Visas from external mproviders.
We need to map our external providers' "ga4gh_passport_v1" scopes into the IMPaCT-Data token:

![](docs/source/content/images/kc-configure-mapper.png)

Choose External IDP Claim Mapper

![](docs/source/content/images/kc-external-mapper.png)

* **Name** - any reasonable name (e.g. "mgi ga4gh_passport_v1 mapper")
* **External IDP** - external IDP Alias
* **External claim name** - ga4gh_passport_v1 (GA4GH Passport claim name)
* **Token claim name** - ga4gh_passport_v1 (GA4GH Passport claim name)

### Authentication with IMPaCT-Data Identity Server.

Because the IMPaCT-Data Identity Server is a standard OpenID Connect 1.0 identity Provider, any compatible clients may be used with it. Some recipes could be found in the Keycloak's [documentation](https://www.keycloak.org/docs/25.0.2/securing_apps/#openid-connect).

__The IMPaCT-Data Galaxy__ instance must be configured for the IMPaCT-Data Keycloak IDP as described in the Galaxy [documentation](https://galaxyproject.org/authnz/config/oidc/#configure-oidc-backends). The docker compose Keycloak installation already includes preconfigured `galaxy.impact-data.bsc.es` client, so the Galaxy `oidc_backends_config.xml` file would look like:
```xml
<?xml version="1.0"?>
<OIDC>
  <provider name="impact">
    <client_id>galaxy.impact-data.bsc.es</client_id>
    <client_secret>1234567890</client_secret>
    <redirect_uri>http://localhost:8080/authnz/impact/callback</redirect_uri>
    <url>http://localhost:8686/auth2/realms/impact-data</url>
    </provider>
</OIDC>
``` 

__Other clients.__  
As if was mentioned before, each platform has it's own OpenID Connect client. Here are some commonly used configurations.
- __WildFly__  
Jakarta Enterprise Edition doesn't specify a standard for the OpenID Connect 1.0 Security. Nevertheless, __WildFly__ server supperts OpenID Connect 1.0 Security via standsrd `/WEB-INF/web.xml` descriptor:

```json
<login-config>
    <auth-method>OIDC</auth-method>
</login-config>

```

and [`/WEB-INF/oidc.json`](https://docs.wildfly.org/33/Admin_Guide.html#deployment-configuration) configuration file.
```json
{
  "provider-url" : "http://localhost:8686/auth2/realms/impact-data",
  "client-id" : "my_impact_client",
  "credentials" : {
      "secret" : "1234567890"
   },
  "ssl-required" : "external",
  ...
}
```
