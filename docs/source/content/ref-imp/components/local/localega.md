# Local EGA

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

El proyecto Local EGA (Local European Genome-Phenome Archive) consta de varios componentes:

> - Una bandeja de entrada
> - Una base de datos con metadatos de archivos
> - Un almacén de archivos a largo plazo
> - Un servicio de ingesta de datos
> - Un sistema de distribución

Su objetivo es resolver el problema de que los datos sensibles no pueden cruzar fronteras (cf. GDPR), mientras que los metadatos públicos sí pueden. Los archivos se almacenarán, una vez cifrados, en EGA locales ubicados en diferentes países, mientras que los metadatos públicos permanecerán en el EGA central.

En resumen, los remitentes depositarán los archivos cifrados en una bandeja de entrada local de EGA, ubicada en el país correspondiente. El proceso de ingesta de datos mueve archivos cifrados desde la bandeja de entrada al almacenamiento a largo plazo y almacena su información de ubicación, así como otros metadatos relacionados con el almacenamiento del archivo en una base de datos. En el proceso, cada archivo ingerido obtiene una ID de acceso, que lo identifica de forma única en todo el ecosistema EGA. El sistema de distribución permite a los solicitantes acceder de forma segura a los archivos cifrados contenidos en el almacén a largo plazo a través de una bandeja de salida, utilizando la identificación de acceso, en el caso de que el Comité de Acceso a Datos de esos archivos le haya concedido acceso a ellos.

:::{figure} ../../../images/local_ega_interaction_schema.png
:align: center
:::
&nbsp;
&nbsp;

## Documentación técnica

### Documentación técnica

+ [https://localega.readthedocs.io/en/latest/](https://localega.readthedocs.io/en/latest/)

### Repositorio de código

+ [https://github.com/EGA-archive/LocalEGA](https://github.com/EGA-archive/LocalEGA)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>
