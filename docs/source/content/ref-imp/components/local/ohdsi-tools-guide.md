# Guía para herramientas OHDSI

## Herramientas OHDSI

**OHDSI** [(*Observational Health Data Sciences and Informatics*)](https://www.ohdsi.org/) es una asociación sin ánimo de lucro que promueve la realización de estudios observacionales con datos clínicos en una configuración federada. Para ello, OHDSI promueve un **modelo común de datos (OMOP CDM)**, y ha desarrollado y ofrece en código abierto distintas herramientas para la configuración, evaluación y control de la calidad y realización de análisis y estudios sobre datos biomédicos integrados en OMOP CDM.

A continuación, detallamos algunas de las herramientas desarrolladas y ofrecidas por OHDSI, y que pueden ser de utilidad en el contexto de los proyectos realizados dentro de la infraestructura de referencia de IMPaCT-Data.

### ATLAS:
- **Descripción**: ATLAS es una aplicación web diseñada para apoyar la creación, ejecución y visualización de análisis observacionales. Facilita la generación de evidencia del mundo real a partir de datos sanitarios.
- **Características**: Permite a los usuarios diseñar cohortes, definir características, realizar estudios de incidencia y prevalencia, y ejecutar análisis comparativos de cohortes, entre otras funcionalidades.
- **Uso**: Ideal para investigadores que necesitan un entorno integrado para el análisis observacional y la generación de hipótesis​ (OHDSI)​​​.
- **Detalle**: ATLAS es una herramienta web gratuíta y accesible públicamente que facilita el diseño y ejecución de análisis sobre datos observacionales a nivel de paciente estandarizados en el formato OMOP CDM. ATLAS se despliega como una aplicación web, junto con el WebAPI de OHDSI. Se suele instalar junto a la base de datos OMOP, y puesto que permite el acceso a datos clínicos del CDM, lo habitual es instalarlo en servidores privados con acceso restringido. Existe, no obstante, un servidor ATLAS público con acceso a unos pocos conjuntos de datos sintéticos, que puede ser utilizado para realizar pruebas o formación sobre ATLAS. ATLAS permite también generar el código R de los análisis realizados, de manera que puede ser luego reproducido en distintas instalaciones que dispongan de datos biomédicos en formato OMOP, sin necesidad de tener instalado ATLAS, o sin necesidad de reconstruir los análisis manualmente.
- **Enlaces**:
    - Documentación: 
      - [Book of OHDSI-ATLAS](https://ohdsi.github.io/TheBookOfOhdsi/OhdsiAnalyticsTools.html#atlas)
      - [ATLAS Wiki](https://github.com/OHDSI/Atlas/wiki) (incluye tutoriales en Youtube)
    - Entorno de pruebas/demo: [http://atlas-demo.ohdsi.org/](http://atlas-demo.ohdsi.org/)
    - Información sobre la instalación: [https://github.com/OHDSI/Atlas/wiki/Atlas-Setup-Guide](https://github.com/OHDSI/Atlas/wiki/Atlas-Setup-Guide)
    - Código fuente en GitHub: [https://github.com/OHDSI/Atlas](https://github.com/OHDSI/Atlas)
    - Video-tutorial (10 min) sobre la creación de cohortes en ATLAS: [https://youtu.be/35jtNdxwVEA](https://youtu.be/35jtNdxwVEA)

:::{figure} ../../../images/ATLAS_screenshot.png
:align: center

*Captura de pantalla de ATLAS*
:::

### ACHILLES:
- **Descripción**: ACHILLES (Automated Characterization of Health Information at Large-scale Longitudinal Evidence Systems) es una herramienta para el análisis y visualización de datos a nivel de población.
- **Características**: Proporciona análisis descriptivos y visualizaciones interactivas que ayudan a comprender mejor los datos de salud. Incluye paneles de control (dashboards) e informes detallados sobre la calidad de los datos y sus características.
- **Uso**: Útil para obtener una visión general rápida y detallada de los datos (catalogación a nivel de detalle de datos), detectar anomalías y evaluar la calidad de los mismos.
- **Detalle**: ACHILLES es un paquete de R que permite caracterizar y visualizar información relativa a un esquema OMOP CDM. ACHILLES genera informes basados en el resumen de datos que genera la función “Data Sources” de ATLAS.
- **Enlaces**:
    - Documentación: 
      - [Book of OHDSI-ACHILLES](https://ohdsi.github.io/TheBookOfOhdsi/DataQuality.html#data-quality-in-general) 
      - [ACHILLES in practice](https://ohdsi.github.io/TheBookOfOhdsi/DataQuality.html#achillesInPractice)
    - Entornos de pruebas/demo:[https://atlas-demo.ohdsi.org/#/datasources](https://atlas-demo.ohdsi.org/#/datasources)
    - Información sobre la instalación:[https://ohdsi.github.io/Achilles/#getting-started](https://ohdsi.github.io/Achilles/#getting-started) 
    - Código fuente en GitHub: [https://github.com/OHDSI/Achilles](https://github.com/OHDSI/Achilles)
    - Video-tutorial (10 min) sobre ACHILLES: [https://www.youtube.com/watch?v=UyS-LAUql-A](https://www.youtube.com/watch?v=UyS-LAUql-A)

:::{figure} ../../../images/ACHILLES_screenshot.png
:align: center

*Captura de pantalla de ACHILLES*
:::

### White Rabbit y Rabbit in a Hat:
- **Descripción**: White Rabbit es una herramienta que analiza bases de datos para proporcionar un resumen del contenido, mientras que Rabbit in a Hat se encarga de facilitar el proceso de diseño y documentación del proceso ETL.
- **Características**: White Rabbit genera perfiles de BBDD que ayudan a planificar la conversión a un modelo de datos común (CDM). Rabbit in a Hat facilita la creación de especificaciones ETL detalladas y documentadas.
- **Uso**: Fundamental para preparar y estandarizar datos antes de su análisis, asegurando que los datos estén en un formato adecuado y compatible con el resto de herramientas de OHDSI​.
- **Detalle**: Ambas herramientas se usan conjuntamente para diseñar el proceso ETL de captura de datos desde las fuentes primarias del usuario hacia un modelo OMOP CDM. Se trata en ambos casos de aplicaciones de escritorio desarrolladas en Java. 
  - WhiteRabbit permite conectarse a múltiples fuentes de datos, ya sea a los servidores de bases de datos (MySQL, SQL Server, Oracle, PostgreSQL…) como a ficheros CSV. WhiteRabbit realiza un escaneo de las fuentes, y genera un informe y un conjunto de ficheros que pueden usarse como punto de partida para Rabbit-in-a-hat.
  - Por su parte, Rabbit-in-a-hat permite establecer y documentar la correspondencia entre las fuentes de datos originales, exploradas previamente con WhiteRabbit, y el modelo OMOP CDM. De esta manera, se fija claramente la correspondencia entre cada campo de cada tabla original y los campos y tablas de OMOP, así como las codificaciones y transformaciones necesarias en el proceso ETL para conseguir un modelo OMOP normalizado. Rabbit-in-a-hat es una herramienta gráfica, que permite definir la relación tabla-tabla y campo-campo de una manera gráfica y sencilla. Rabbit-in-a-hat no permite generar el código para realizar el proceso ETL (de momento), pero sí es una ayuda inestimable para documentar el proceso y proceder a su implementación utilizando la herramienta de elección del usuario.
- **Enlaces**:
    - Documentación: 
      - [Book of OHDSI – WhiteRabbit](https://ohdsi.github.io/TheBookOfOhdsi/ExtractTransformLoad.html#white-rabbit)
      - [Book of OHDSI – Rabbit-In-A-Hat](https://ohdsi.github.io/TheBookOfOhdsi/ExtractTransformLoad.html#rabbit-in-a-hat)
    - Sitios Web: 
      - [WhiteRabbit](https://ohdsi.github.io/WhiteRabbit/WhiteRabbit.html)
      - [Rabbit-In-A-Hat](https://ohdsi.github.io/WhiteRabbit/RabbitInAHat.html)
    - Información sobre la instalación:
      - [WhiteRabbit](https://ohdsi.github.io/WhiteRabbit/WhiteRabbit.html#installation_and_support)
      - [Rabbit-In-A-Hat](https://ohdsi.github.io/WhiteRabbit/RabbitInAHat.html#installation_and_support)
    - Código fuente en GitHub
      - [WhiteRabbit y Rabbit-In-A-Hat](https://github.com/OHDSI/WhiteRabbit)

:::{figure} ../../../images/Rabbit_screenshot.png
:align: center

*Captura de pantalla de Rabbit-In-A-Hat*
:::

### USAGI:
- **Descripción**: Se trata de una aplicación diseñada para mapear códigos locales de datos de salud a vocabularios estandarizados como SNOMED, LOINC y RxNorm.- **Características**: Utiliza algoritmos avanzados para sugerir mapeos, lo que facilita la normalización de los datos antes de su análisis.
- **Uso**: Esencial para garantizar que los datos estén estandarizados y sean interoperables, lo que a su vez resulta fundamental para los análisis comparativos y estudios multicéntricos​.
- **Detalle**: Es una aplicación en entorno web, de instalación local, que permite, a partir de los vocabularios normalizados de OMOP, buscar las mejores correspondencias entre los códigos y terminologías locales (sean estándar o no) y los códigos estándar de OMOP. Tiene la limitación, dado que trabaja con los vocabularios estandarizados de OMOP, de realizar las búsquedas solo sobre términos en inglés, por lo que el mapeo entre códigos OMOP y terminologías locales en español, requieren de una traducción previa de los términos españoles al inglés.
- **Enlaces**:
    - Documentación: 
      - [Book of OHDSI - USAGI](https://ohdsi.github.io/TheBookOfOhdsi/ExtractTransformLoad.html#usagi)
    - Información sobre la instalación: [https://github.com/OHDSI/Usagi#getting-started](https://github.com/OHDSI/Usagi#getting-started)
    - Código fuente en GitHub: [https://github.com/OHDSI/Usagi](https://github.com/OHDSI/Usagi)
    - Video-tutorial (10 min) sobre USAGI: [https://www.youtube.com/watch?v=O65_c3UX8Zs](https://www.youtube.com/watch?v=O65_c3UX8Zs)

:::{figure} ../../../images/USAGI_screenshot.png
:align: center

*Captura de pantalla de USAGI*
:::

### WebAPI:
- **Descripción**: Se encarga de proporcionar la funcionalidad backend para aplicaciones como ATLAS, permitiendo la ejecución de análisis y la interacción con datos almacenados en un CDM.
- **Características**: Incluye servicios web para gestionar cohortes, conceptos y ejecutar varios tipos de análisis observacionales.
- **Uso**: Permite a los desarrolladores integrar y automatizar procesos de análisis de datos, facilitando la creación de aplicaciones personalizadas basadas en la plataforma OHDSI​​.
- **Detalle**: OHDSI WebAPI provee un conjunto de servicios RESTful que pueden ser invocados por las aplicaciones OHDSI. Provee un API centralizado para conectarse con una o varias bases de datos OMOP CDM, permite buscar vocabularios OMOP estandarizados, definir y caracterizar cohortes, calcular tasas de incidencia, o diseñar estimación poblacionales o estudios predictivos a nivel de paciente entre otras funcionalidades. WebAPI es una aplicación desarrollada en Java 8 que utiliza una base de datos PostgreSQL para su almacenamiento local.
- **Enlaces**:
    - Documentación: [http://webapidoc.ohdsi.org/](http://webapidoc.ohdsi.org/)
    - Información sobre la instalación: [https://github.com/OHDSI/WebAPI/wiki](https://github.com/OHDSI/WebAPI/wiki)
    - Código fuente en GitHub: [https://github.com/OHDSI/WebAPI](https://github.com/OHDSI/WebAPI)

### ATHENA:
- **Descripción**: Portal web para la búsqueda y exploración de términos clínicos y códigos normalizados utilizados en OMOP CDM.
- **Características**: Permite explorar y navegar a través de todos los estándares de codificación utilizados en OMOP, así como seleccionar y descargar los vocabularios deseados para su utilización en los modelos de datos locales.
- **Uso**: Es una herramienta fundamental, junto con USAGI, para el proceso de mapeo entre códigos locales y códigos normalizados en OMOP, así como para explorar las relaciones entre las distintas codificaciones.
- **Detalle**: ATHENA es una explorador terminológico en entorno Web desplegado sobre la infraestructura de OHDSI. Aunque es posible obtener el código fuente y desplegarlo en una instancia local, no merece la pena, y resulta más sencillo utilizar directamente la instancia ofrecida por OHDSI. El portal web permite realizar búsquedas en multitud de vocabularios clínicos y de uso general, tanto por descriptivo como por código, y ofrece todas las correspondencias entre vocabularios, y sus relaciones ascendentes y descendentes. El buscador permite, a su vez, filtrar los términos encontrados por múltiples dimensiones, lo que facilita enormemente la identificación de los conceptos estandarizados que mejor se ajustan a los códigos locales utilizados en los conjuntos de datos de partida.
- **Enlaces**:
    - Buscador [ATHENA](https://athena.ohdsi.org/search-terms/terms)
    - Código fuente en GitHub: [https://github.com/OHDSI/Athena](https://github.com/OHDSI/Athena)
    - Video-tutorial (10 min) sobre ATHENA: [https://www.youtube.com/watch?v=2WdwBASZYLk](https://www.youtube.com/watch?v=2WdwBASZYLk)

:::{figure} ../../../images/ATHENA_screenshot.png
:align: center

*Captura de pantalla de ATHENA*
:::

## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>