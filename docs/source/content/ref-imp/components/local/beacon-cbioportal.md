# Implementación de Beacon-cBioportal

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

B4cBioPortal (*Beacon for cBioportal*) se desarrolla a partir del software {doc}`B2RI <beacon>`, lo que permite la integración de Beacon sobre una API (del inglés, *application programming interface*) de cBioPortal. Este proyecto está liderado por el Barcelona Supercomputing Center-Centro Nacional de Supercomputación (BSC-CNS), con el asesoramiento del Instituto Hospital del Mar de Investigación (IMIM), el Centro de Regulación Genómica (CRG) y la Fundación para la Investigación del Hospital Clínico de la Comunidad Valenciana (INCLIVA).

[cBioPortal](https://www.cbioportal.org/) es una plataforma en línea diseñada para visualizar y analizar datos complejos de genómica del cáncer. Proporciona una interfaz fácil de usar para que investigadores y médicos exploren amplios conjuntos de datos sobre genómica del cáncer. Ampliamente utilizado en la comunidad de investigación, cBioPortal ayuda a comprender la genómica del cáncer, identificar posibles objetivos terapéuticos y tomar decisiones informadas en el tratamiento y la investigación del cáncer.

Incluye datos disponibles públicamente de fuentes como [The Cancer Genome Atlas](https://www.cancer.gov/ccg/research/genome-sequencing/tcga) (TCGA) y otros proyectos importantes de genómica del cáncer, lo que lo convierte en un recurso valioso para los investigadores del cáncer.

B4cBioPortal se puede implementar manualmente o mediante un contenedor de [Singularity](https://docs.sylabs.io/guides/3.5/user-guide/introduction.html). Requiere un enlace a la API de cBioPortal para funcionar, ya que opera con consultas POST. La utilización de cBioPortal para consultar datos elimina la necesidad de convertir los datos de cBioPortal al formato Beacon. Dado que el modelo de datos de cBioPortal es flexible con pocas variables obligatorias, IMPaCT-Data ha creado un modelo de datos sugerido basado en TCGA. Al utilizar este modelo como plantilla para las implementaciones, se puede implementar un Beacon directamente asignando las variables de cBioPortal a las propiedades del Beacon a través de un archivo de configuración.

## Documentación técnica

### Documentación técnica

+ [https://gitlab.bsc.es/impact-data/impd-beacon_cbioportal/-/blob/main/README.md](https://gitlab.bsc.es/impact-data/impd-beacon_cbioportal/-/blob/main/README.md)

### Repositorio de código

+ [https://gitlab.bsc.es/impact-data/impd-beacon_cbioportal](https://gitlab.bsc.es/impact-data/impd-beacon_cbioportal)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>
