# Guía sobre cBioportal

## Descripción del componente

Este documento ofrece recomendaciones para crear instancias de cBiportal para instituciones que buscan:

1. Implementar cBioPortal con sus propios conjuntos de datos, ya sean de acceso público o alojados en instancias privadas, para hacerlos interoperables con conjuntos de datos alojados en la misma instancia de cBioPortal o en otras, 

2. Hacerlos reconocibles con el protocolo Beacon v2. 

En este documento se discute el tema de la interoperabilidad, particularmente en el campo del cáncer donde cBioPortal se usa comúnmente, y comparamos diferentes modelos de datos, identificando un conjunto mínimo de variables comunes. También describimos los pasos técnicos necesarios para implementar cBioPortal, con (o sin) un Beacon en la parte superior, para garantizar que los conjuntos de datos sean interoperables y capaces de integrarse sin problemas en una red Beacon (por ejemplo, IMPaCT Beacon Network), haciéndolos así fácilmente detectables y accesibles. a una audiencia más amplia.

## cBioPortal

[cBioPortal](https://www.cbioportal.org/) es una plataforma digital y una herramienta web para la visualización y análisis de datos genómicos complejos de pacientes con cáncer. Proporciona una interfaz fácil de usar para que investigadores y médicos accedan y exploren conjuntos de datos genómicos de pacientes con cáncer a gran escala. cBioPortal es ampliamente utilizado por la comunidad de investigación y los médicos para obtener información sobre la genómica del cáncer, descubrir posibles objetivos terapéuticos y tomar decisiones más informadas en el tratamiento y la investigación del cáncer. cBioPortal también incluye datos disponibles públicamente de diversas fuentes, como [*The Cancer Genome Atlas*](https://www.cancer.gov/ccg/research/genome-sequencing/tcga) (TCGA) y otros proyectos de genómica del cáncer a gran escala, lo que lo convierte en un recurso valioso para todos aquellos investigadores estudiando el cáncer.

## Interoperabilidad

cBioPortal ofrece una flexibilidad casi total a la hora de decidir qué variables incluir en un conjunto de datos. Esto puede ser una gran ventaja en algunos sentidos, ya que permite a los investigadores personalizar sus instancias de cBioPortal sin restricciones. Desde esa perspectiva, esto es particularmente útil para datos clínicos que a menudo se organizan de manera diferente a partir de diferentes fuentes. Sin embargo, esta flexibilidad es un arma de doble filo, ya que crea problemas importantes para la interoperabilidad entre conjuntos de datos o cohortes, lo que dificulta la reutilización de los datos en análisis comparativos.

El uso de cBioPortal en escenarios federados se ha explorado recientemente, con proyectos como **IMPaCT-Data** y [EOSC4Cancer](https://eosc4cancer.eu/), que lo adoptaron y recomendaron oficialmente. Por lo tanto, existe una gran necesidad por parte de la comunidad de acordar un modelo común o algunas variables mínimas al usar cBioPortal, ya que el objetivo final es tener los datos lo más FAIR (Encontrables, Accesibles, Interoperables, Reutilizables) posible. 

De manera similar, **Beacon v2** se está adoptando ampliamente en numerosos proyectos (incluidos **IMPaCT** y **EOSC4Cancer**, pero también **GDI** y **EUCAIM**) como herramienta de descubrimiento en diferentes dominios y para diferentes tipos de datos más allá de los datos genómicos y clínicos (por ejemplo, registros de cáncer o imágenes médicas). Al "beaconizar" los datos, esos datos se vuelven más interoperables y, al implementar un Beacon o unirse a una red Beacon, a su vez, los datos son más FAIR. 

Por ello, en esta guía comparamos diferentes modelos de datos en cáncer para identificar un conjunto mínimo común de variables que podrían considerarse un modelo de datos mínimo para hacer que las instancias de cBioPortal sean interoperables entre sí, teniendo también en cuenta el modelo Beacon para, en última instancia, "beaconizar". estos datos también. Como parte de esta **Implementación de Referencia** se ha desarrollado una prueba técnica de concepto para un Beacon directamente en una instancia de cBioPortal ({doc}`Beacon-cBioportal (Genómico-clínico) <beacon-cbioportal>`).

## Objetivos

1. Mejorar la capacidad de descubrimiento de cohortes de pacientes con cáncer alojadas en cBioPortal utilizando el potencial de Beacon.
2. Explorar la reutilización de modelos de datos nuevos o populares mediante comparación e identificando puntos en común.
3. Proporcionar recomendaciones para la interoperabilidad al implementar cBioPortal.
4. Desarrollar una prueba técnica de concepto para una Beacon implementada directamente sobre cBioPortal.
5. Proporcionar instrucciones para implementar cBioPortal y Beacon v2 en sus propios conjuntos de datos.

## Documentación técnica

+ Enlace al documento de la Guía sobre cBioPortal: [Guía sobre cBioportal](hhttps://b2drop.bsc.es/index.php/f/3260226)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>