# Entorno Virtual openVRE

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

### Qué es openVRE

openVRE (de open Virtual Research Environment) es un entorno de trabajo basado en la nube que permite construir rápidamente su propia plataforma computacional. Ofrece:

- una interfaz web fácil de usar que integra una serie de recursos conectables:
 - herramientas de análisis o canalizaciones
 - interfaces para repositorios de datos externos
 - visualizadores
- un backend escalable para computación en la nube compatible con middlewares OCCI como OpenNebula u OpenStack.

### Arquitectura

El repositorio principal contiene el código fuente de openVRE, incluyendo un front-end web personalizable y un código central que administra datos, metadatos, unidades de procesamiento (herramientas de análisis y visualización) y recursos computacionales. El siguiente diagrama ilustra un posible diseño para una infraestructura computacional basada en openVRE:

:::{figure} ../../../images/openvre_schema.png
:align: center
:::
&nbsp;
&nbsp;

Los diferentes componentes trabajan juntos para proporcionar un entorno computacional para herramientas de análisis o visualización iniciadas por los usuarios a través de la interfaz web de VRE. Las solicitudes de los usuarios son procesadas por uno de los dos programadores disponibles: PMES o SGE-oneflow. Estos programadores activan las herramientas dentro de las máquinas virtuales (VM) adecuadas donde se encapsulan las herramientas seleccionadas. Los recursos se asignan dinámicamente en función de la demanda del usuario, y la cantidad de VM dedicadas se escala de acuerdo con las solicitudes de trabajo, según lo permitan los recursos computacionales en la nube. Estas VM acceden a varios volúmenes de datos donde se almacenan los archivos de entrada (ya sean privados (del espacio de trabajo del usuario) o conjuntos de datos públicos) y donde se escriben los archivos de salida.

Una vez que una solicitud de trabajo llega a la VM asignada, comienza la ejecución de la herramienta. Las herramientas VRE integradas son componentes de software independientes desarrollados por terceros (por ejemplo, bioinformáticos, investigadores, estadísticos) y se implementan en las VM locales de la plataforma de cómputo. Todas las herramientas se adhieren a la estructura común definida por openvre-tool-api, lo que permite que el núcleo openVRE las active de manera uniforme.

## Documentación técnica

### Documentación técnica

[https://github.com/inab/openVRE/wiki](https://github.com/inab/openVRE/wiki)

### Cómo instalar

+ [Instalación via Docker](https://github.com/inab/dockerized_vre/blob/hpc_access/Install.md)
+ [Instalar y ejecutar una herramienta](https://github.com/inab/openVRE/wiki/Bring-your-own-tool)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>