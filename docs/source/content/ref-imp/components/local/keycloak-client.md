# Cliente de Autenticación/Autorización (Keycloak implementation)

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

### Keycloak

KeyCloak es una herramienta de gestión de acceso e identidad de código abierto. Con KeyCloak puede agregar autenticación a aplicaciones y servicios seguros con el mínimo esfuerzo y sin la necesidad de almacenar usuarios o autenticarlos.
Keycloak proporciona federación de usuarios, autenticación sólida, administración de usuarios, autorización detallada y más:

> - Inicio de sesión único
>   : Los usuarios se autentican con Keycloak en lugar de aplicaciones individuales. Esto significa que sus aplicaciones no tienen que lidiar con formularios de inicio de sesión, autenticación de usuarios y almacenamiento de usuarios. Una vez que hayan iniciado sesión en Keycloak, los usuarios no tendrán que volver a iniciar sesión para acceder a una aplicación diferente.
>
>     Esto también se aplica al cerrar sesión. Keycloak proporciona cierre de sesión único, lo que significa que los usuarios solo tienen que cerrar sesión una vez para cerrar sesión en todas las aplicaciones que utilizan Keycloak.
> - Intermediación de identidad e inicio de sesión social
>   : Habilitar el inicio de sesión con redes sociales es fácil de agregar a través de la consola de administración. Sólo es cuestión de seleccionar la red social que deseas agregar. No se requiere código ni cambios en su aplicación.
>
>     Keycloak también puede autenticar usuarios con OpenID Connect o proveedores de identidad SAML 2.0 existentes. Nuevamente, esto es solo una cuestión de configurar el proveedor de identidad a través de la consola de administración.
> - Federación de usuarios
>   : Keycloak tiene soporte integrado para conectarse a servidores LDAP o Active Directory existentes. También puedes implementar tu propio proveedor si tienes usuarios en otras tiendas, como una base de datos relacional.

:::{figure} ../../../images/keycloak_schema.png
:align: center
:::
&nbsp;
&nbsp;

### Aplicaciones seguras

Como servidor compatible con OAuth2, OpenID Connect y SAML, Keycloak puede proteger cualquier aplicación y servicio siempre que la pila de tecnología que utilice admita cualquiera de estos protocolos. Para obtener más detalles sobre los protocolos de seguridad admitidos por Keycloak, considere consultar la Guía de administración del servidor.

La mayor parte del soporte para algunos de estos protocolos ya está disponible en el lenguaje de programación, el marco o el proxy inverso que están utilizando. Aprovechar el soporte que ya está disponible en el ecosistema de aplicaciones es un aspecto clave para que su aplicación cumpla totalmente con los estándares de seguridad y las mejores prácticas, de modo que evite depender de un proveedor.

Para algunos lenguajes de programación, Keycloak proporciona bibliotecas que intentan llenar el vacío por la falta de soporte de un protocolo de seguridad en particular o proporcionar una integración más rica y estrechamente acoplada con el servidor. Estas bibliotecas son conocidas por los adaptadores de cliente Keycloak y deben usarse como último recurso si no puede confiar en lo que está disponible en el ecosistema de aplicaciones.

#### Ejemplos de securización de aplicaciones

##### Autenticación con el Servidor de Identidad IMPaCT-Data.

Debido a que el Servidor de Identidad IMPaCT-Data es un proveedor de identidad estándar OpenID Connect 1.0, se pueden utilizar cualquier cliente compatible con él. Algunas recetas se pueden encontrar en la [documentación](https://www.keycloak.org/docs/25.0.2/securing_apps/#openid-connect) de Keycloak.

__La instancia de IMPaCT-Data Galaxy__ debe ser configurada para el IDP Keycloak de IMPaCT-Data como se describe en la [documentación](https://galaxyproject.org/authnz/config/oidc/#configure-oidc-backends) de Galaxy. La instalación de Keycloak con Docker Compose ya incluye el cliente preconfigurado `galaxy.impact-data.bsc.es`, por lo que el archivo `oidc_backends_config.xml` de Galaxy se vería así:
```xml
<?xml version="1.0"?>
<OIDC>
  <provider name="impact">
    <client_id>galaxy.impact-data.bsc.es</client_id>
    <client_secret>1234567890</client_secret>
    <redirect_uri>http://localhost:8080/authnz/impact/callback</redirect_uri>
    <url>http://localhost:8686/auth2/realms/impact-data</url>
    </provider>
</OIDC>
``` 

__Otros clientes.__  
Como se mencionó antes, cada plataforma tiene su propio cliente OpenID Connect. Aquí hay algunas configuraciones comúnmente utilizadas.
- __WildFly__  
Jakarta Enterprise Edition no especifica un estándar para la Seguridad OpenID Connect 1.0. Sin embargo, el servidor __WildFly__ admite la Seguridad OpenID Connect 1.0 a través del descriptor estándar `/WEB-INF/web.xml`:

```json
<login-config>
    <auth-method>OIDC</auth-method>
</login-config>

```

y el fichero de configuración [`/WEB-INF/oidc.json`](https://docs.wildfly.org/33/Admin_Guide.html#deployment-configuration).
```json
{
  "provider-url" : "http://localhost:8686/auth2/realms/impact-data",
  "client-id" : "my_impact_client",
  "credentials" : {
      "secret" : "1234567890"
   },
  "ssl-required" : "external",
  ...
}
```

## Documentación técnica

### Documentación técnica

+ [https://www.keycloak.org/docs/latest/securing_apps/](https://www.keycloak.org/docs/latest/securing_apps/)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>