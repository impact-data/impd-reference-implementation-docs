# Implementación de Beacon-OMOP

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

El B4OMOP (*Beacon for OMOP*) es un desarrollo derivado del software {doc}`B2RI <beacon>`, que permite la integración de un Beacon en cualquier base de datos [CDM de OMOP](https://www.ohdsi.org/data-standardization/), liderado por BSC-CNS (*Barcelona Supercomputing Center*-Centro Nacional de Supercomputación) con la orientación y soporte de CRG (*Centre de Regulació Genómica*). El modelo de datos común (CDM) de Observational Medical Outcomes Partnership (OMOP) es una estructura de base de datos estandarizada para organizar y analizar datos de atención médica de diferentes fuentes. Esta base de datos de uso común está diseñada para facilitar la investigación observacional a gran escala y tiene como objetivo permitir a los investigadores generar evidencia confiable para la toma de decisiones en materia de atención médica. OMOP CDM organiza los datos en tablas estandarizadas con formatos predefinidos, lo que facilita la armonización de datos de fuentes dispares y la realización de análisis en diversos conjuntos de datos de atención médica.

:::{figure} ../../../images/beacon_omop_schema.png
:align: center
:::

El procedimiento de implementación funciona de la misma manera que el B2RI, donde iniciar el contenedor o realizar una instalación manual configura instantáneamente una Beacon. Sin embargo, ahora la aplicación tiene una infraestructura de backend diferente, donde ahora la baliza consulta una base de datos relacional basada en OMOP CDM.

El mapeo experto entre los modelos OMOP CDM y Beacon garantiza la alineación de elementos como diagnósticos, tratamientos, resultados de laboratorio, exposiciones y datos de muestras biológicas.

Al utilizar OMOP CDM como base para consultar datos, se elimina la necesidad de convertir datos al formato Beacon Friendly (BFF), dado que los datos ya se adhieren a un modelo reconocido. Por lo tanto, las instituciones equipadas con un CDM OMOP pueden implementar un Beacon en cuestión de minutos.

:::{figure} ../../../images/beacon_omop_insight_schema.png
:align: center
:::

## Documentación técnica

### Documentación técnica

+ [https://gitlab.bsc.es/impact-data/impd-beacon_omopcdm/-/blob/main/README.md](https://gitlab.bsc.es/impact-data/impd-beacon_omopcdm/-/blob/main/README.md)

### Repositorio de código

+ [https://gitlab.bsc.es/impact-data/impd-beacon_omopcdm](https://gitlab.bsc.es/impact-data/impd-beacon_omopcdm)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>
