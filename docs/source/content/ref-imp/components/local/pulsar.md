# Entorno Virtual Galaxy + Pulsar

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

### Pulsar

Para optimizar la infraestructura del Entorno Virtual Galaxy + Pulsar, en lugar de tener que instalar varios nodos centrales UseGalaxy, la instalación local de este componente tan solo requiere de la instalación de un nodo UseGalaxy Pulsar

UseGalaxy Pulsar es un sistema de ejecución distribuida que permite a Galaxy ejecutar trabajos en sistemas remotos. Con Pulsar, los datos de entrada y los diferentes scripts y archivos de configuración se pueden transferir al sistema remoto, donde se ejecuta el trabajo de análisis y los resultados se transfieren nuevamente al servidor Galaxy, eliminando la necesidad de un sistema de archivos compartido.

Las principales ventajas de utilizar un nodo Pulsar para la ejecución distribuida de trabajos son:

 + Escalabilidad: Pulsar permite a Galaxy descargar tareas computacionales a recursos remotos.
 + Gestión de recursos: Ayuda a gestionar los recursos computacionales distribuyendo las tareas en función de la disponibilidad y los requisitos de los recursos.

:::{figure} ../../../images/pulsar_galaxy_schema.png
:align: center
:::
&nbsp;
&nbsp;

## Documentación Técnica

### Instalación de Pulsar

De la misma manera que se ha llevado a cabo con el servidor central de Galaxy, la implementación de pulsar para el proyecto  IMPaCT-Data se realiza a través de Ansible con las [recetas de implementación](https://gitlab.bsc.es/impact-data/impact-pulsar) proporcionadas.

La documentación oficial donde se refleja como se forma el playbook paso
a paso se encuentra en:

 + [Tutorial oficial](https://training.galaxyproject.org/training-material/topics/admin/tutorials/pulsar/tutorial.html)

### Repositorio de código

 + [https://gitlab.bsc.es/impact-data/impact-pulsar](https://gitlab.bsc.es/impact-data/impact-pulsar)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>