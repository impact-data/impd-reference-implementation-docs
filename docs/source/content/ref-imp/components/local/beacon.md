# El Protocolo Beacon

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

% beacon:

[Beacon v2](https://www.google.com/url?q=https://genomebeacons.org/&sa=D&source=docs&ust=1730113572013452&usg=AOvVaw1Tx7_TV_utLUNZSvIPdunm) es una **especificación API** establecida por la **Iniciativa de la Alianza Global para la Genómica y la Salud** ([GA4GH](https://www.ga4gh.org/)) que define un estándar abierto para el descubrimiento federado. de datos genómicos y fenotípicos en investigación biomédica y aplicaciones clínicas.

La [versión actual de la especificación es v2](https://github.com/ga4gh-beacon/beacon-v2) y consta de dos componentes, el **Framework** y los **Modelos**. El Framework define el formato para las solicitudes y respuestas, mientras que los Modelos definen la [estructura](https://json-schema.org/specification-links.html#2020-12) de la respuesta de datos biológicos. La función general de estos dos componentes es proporcionar instrucciones para diseñar una **API REST** (interfaz de programación de aplicaciones de transferencia de estado representacional) con la especificación **OpenAPI** (OAS). La OEA define una interfaz estándar independiente del idioma que utilizan los desarrolladores de software para implementar API REST.

:::{figure} ../../../images/beacon_schema.png
:align: center

*Representación esquemática de cómo funciona Beacon.*
:::

### Diferentes instancias de Beacon

Para el desarrollo del proyecto IMPaCT-Data se ha utilizado la implementación estándar de Beacon v2: 
+ [Beacon v2 - Implementación de Referencia (Genómico-clínico)](https://github.com/EGA-archive/beacon2-ri-api)

Dentro del proceso de desarrollo de IMPaCT-Data se han generado dos instancias Beacon específicas, basadas en la implementación de referencia:

+ {doc}`Beacon-OMOP (Clínico) <beacon-omop>`
+ {doc}`Beacon-cBioPortal (Genómico-clínico) <beacon-cbioportal>`

## Documentación técnica

### Documentación técnica

+ [Documentación de Beacon v2 Reference Implementation](https://b2ri-documentation-demo.ega-archive.org/)

### Repositorio de código

La instancia enlazada a continuación es la que se ha usado en el desarrollo de IMPaCT-Data. Esta es la instacia que se debe utilizar a la hora de probar el componente, así como sus extensiones de {doc}`Beacon-OMOP`<beacon-ompop> y {doc}`Beacon-cBioPortal`<beacon-cbioportal>:

+ [Beacon v2 - Implementación de Referencia (Genómico-clínico)](https://github.com/EGA-archive/beacon2-ri-api)

## Enlaces adicionales

### Repositorio de código - Instancia de Producción

La instancia enlazada a continuación está en producción y está actualmente en pruebas. La documentación técnica de esta instancia no está acabada completamente, pero será muy similar a la documentación de la implementación de referencia:

+ [Repositorio de instancia en producción](https://github.com/EGA-archive/beacon2-pi-api)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>

