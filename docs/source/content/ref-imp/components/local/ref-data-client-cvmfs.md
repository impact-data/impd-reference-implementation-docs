# Cliente de referencia CVMFS

```{contents} ÍNDICE
:depth: 3
```

## Descripción del componente

CVMFS (CERN Virtual Machine File System) es un sistema de archivos (filse system) especializado para entornos informáticos distribuidos a gran escala.

Este componente puede contener datos de múltiples naturalezas y orígenes. Para el servidor Galaxy, el componente instalado es uno específico que contiene:

 + Datos de referencia: Secuencias de genoma de cientos de especies, índices para las diferentes secuencias del genoma, índices de herramientas bioinformáticas para los genomas disponibles, etc.

 + Contenedores de herramientas: Contenedores de singularity de todas las  herramientas almacenadas en Biocontainers.

:::{figure} ../../../images/cvmfs_schema.svg
:align: center
:::
&nbsp;
&nbsp;

## Documentación técnica

### Documentación técnica

La implementación de modulo de CVMFS para IMPaCT-Data se realiza a través de Ansible. 

Aunque IMPaCT-Data WP2 proporciona unas [recetas de implementación](https://gitlab.bsc.es/impact-data/galaxy-cvmfs), es importante tener en cuenta que los requisitos para una implementación exitosa del componente pueden variar de un nodo a otro.

Para ir paso a paso a través del proceso de un servidor UseGalaxy, los siguientes tutoriales prácticos oficiales:
 + [Tutorial oficial](https://training.galaxyproject.org/training-material/topics/admin/tutorials/cvmfs/tutorial.html)

### Repositorio de código

+ [https://gitlab.bsc.es/impact-data/galaxy-cvmfs](https://gitlab.bsc.es/impact-data/galaxy-cvmfs)


## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>