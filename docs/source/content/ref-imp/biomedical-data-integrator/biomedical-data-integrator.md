# Integrador de datos biomedicos

## Descripción del integrador

Uno de los objetivos técnicos de IMPaCT-Data es el desarrollo de **prototipos de integración de los resultados del análisis genómico y de imagen con la información normalizada extraída de las Historias Clínicas Electrónicas (HCE)**. Estos prototipos ponen a disposición del investigador herramientas que le permitan descubrir, mediante consultas integradas, si los repositorios de datos disponen de los datos necesarios para realizar un determinado estudio. Una vez detectados los datos de interés, el investigador deberá solicitar el acceso de acuerdo con lo que marca la regulación y las normativas de acceso a datos del repositorio y si éste es concedido, los datos se podrán descargar para hacer uso de herramientas de análisis y visualización para llevar a cabo el estudio autorizado:

:::{figure} ../../images/data_integrator_schema.png
:align: center

*Representación esquemática de cómo funciona el integrador de datos biomédicos.*
:::

Una de las etapas principales en el proceso de integración es la etapa de descubrimiento, tal y como se ha mencionado. En dicha etapa, se pueden hacer uso de soluciones tipo Beacon. El protocolo Beacon es una especificación impulsada por la iniciativa *Global Alliance for Genomics and Health (GA4GH)*, que define un estándar abierto para el descubrimiento de datos genómicos y fenotípicos en investigación biomédica y aplicaciones clínicas. Sin embargo, este protocolo puede ser adaptado a otro tipo de datos, como los datos clínicos o los datos de imagen médica.

En este sentido, se han utilizado diferentes instancias del protocolo Beacon para diferentes tipos de datos (genómico, clínico y de imagen médica) con el fin de desarrollar una herramienta de descubrimiento mediante consultas integradas. Estas instancias se ofrecen como componentes locales en la implementación de referencia de IMPaCT-Data:

- Beacon v2, para el descubrimiento de datos genómicos: [Beacon v2](https://impact-data-ref-imp.readthedocs.io/es/latest/content/ref-imp/components/local/beacon.html)
- Beacon-OMOP (B4OMOP), para el descubrimiento de datos clínicos: [B4OMOP](https://impact-data-ref-imp.readthedocs.io/es/latest/content/ref-imp/components/local/beacon-omop.html) 
- Beacon Imagen Médica (B4IMAGES), para el descubrimiento de datos de imagen médica: [B4IMAGES](https://impact-data-ref-imp.readthedocs.io/es/latest/content/ref-imp/components/local/beacon-image.html)

El integrador de datos biomédicos hace uso de estos tres componentes para el descubrimiento de datos genómicos, clínicos y de imagen en un caso de uso concreto. Este ejemplo está compuesto por datos genómicos de coronavirus secuenciados en el circuito de secuenciación genómico de Andalucía [https://www.clinbioinfosspa.es/COVID_circuit](https://www.clinbioinfosspa.es/COVID_circuit) donde hasta la fecha se han secuenciado más de 42.000 genomas, datos clínicos de los pacientes a los que se les ha secuenciado el virus y datos de imagen asociados a esos pacientes, en este caso, sintéticos. 

:::{figure} ../../images/data_integrator_image1.png
:align: center

*Integrador de datos biomédicos disponible en [https://www.clinbioinfosspa.es/tools/virus-beacon/](https://www.clinbioinfosspa.es/tools/virus-beacon/)*
:::

Así:

- Para el *beacon genómico* (Beacon v2) se ha hecho uso de un subconjunto de 319 genomas de SARS-CoV-2 secuenciados en el Hospital Virgen del Rocío en el contexto del circuito de secuenciación mencionado. Sobre el beacon genómico, se puede realizar la consulta de la ausencia o presencia de una determinada variante genómica (en formato nucleotídico o proteico) o rango de posiciones. Este beacon proporciona al usuario información relativa a la variante genómica, como el gen donde ocurre, la consecuencia y el número de individuos de la cohorte que la contienen.

- Para el *beacon clínico* (B4OMOP) se ha hecho uso de los datos clínicos limitados de los 319 pacientes a los que se secuenció el virus. Se dispone de información del género del paciente, de la historia clínica del mismo o de medidas específicas correspondientes a diferentes pruebas analíticas. Esta información clínica en formato OMOP, ha sido proporcionada por el Grupo de Innovación Tecnológica del Hospital Virgen del Rocío.

- Para el *beacon de imagen médica* (B4IMAGES) se ha construido un conjunto de datos sintéticos para los 319 individuos, disponiendo información sobre el diagnóstico realizado sobre la imagen médica y el lugar anatómico donde se tomó esa imagen. Así, se han generado un total de 626 entradas de diagnósticos y 630 entradas de pruebas diagnósticas. 


## Enlace a integrador

[https://www.clinbioinfosspa.es/tools/virus-beacon/](https://www.clinbioinfosspa.es/tools/virus-beacon/)

## Enlace a documentación

[https://www.clinbioinfosspa.es/tools/virus-beacon/about](https://www.clinbioinfosspa.es/tools/virus-beacon/about)


```{note}
El componente B4IMAGES se ha usado para validar el integrador, pero todavía está en desarrollo.
```

## Contacto

Para cualquier duda durante el periodo de uso y validación de los componentes de la Implementación de Referencia de IMPaCT-Data (Marzo, 2025), podéis poneros en contacto con: <span id="email"></span>

<script>
  const part1 = "&#105;&#109;&#112;&#97;&#99;&#116;&#45;&#100;&#97;&#116;&#97;&#46;&#99;&#111;&#111;&#114;&#100;";
  const part2 = "&#98;&#115;&#99;";
  const part3 = "&#46;&#101;&#115;";
  document.getElementById("email").innerHTML = `<a href="mailto:${part1}@${part2}${part3}">${part1}@${part2}${part3}</a>`;
</script>
